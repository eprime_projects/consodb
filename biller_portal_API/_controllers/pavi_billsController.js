const sql = require("mssql/msnodesqlv8");
 
const config = {
    // user: 'dev_jhon',
    // password: 'Abc123456',
    //server: '10.15.226.112\\SQLSVRCONSOSTG',
    server: 'EPRIME-JHON\\CONSODASHBOARD',
    database: 'PAVI_Bills',   
    driver: 'msnodesqlv8',
    requestTimeout: 300000,
    options : {
        encrypt: false,
        trustServerCertificate: true,
        trustedConnection: true
        }
};
const pool = new sql.ConnectionPool(config);

pool.connect().then(() => {
  return true;
}).catch((err) => {
  console.error('Error connecting to the database:', err);
});

// Parameterized
module.exports.TotalSynchedBills = async (reqBody) => {
  const request = pool.request();
  const result = await request
    .input('UserbillerCode', sql.Int, reqBody.BillerCode)
    .query(`
      SELECT
        COUNT(a.id) as TotalCount
      FROM 
        [t_Bills] a
      LEFT JOIN [CustomerPortal].[dbo].[Bills] b 
        ON a.cBankRef = b.Atmref
        AND a.cBillDate = b.BillingMonth
      WHERE
        a.cBillDate = CONVERT(varchar(7), GETDATE(), 111) AND
        a.cModifiedDate = GETDATE()
        ${reqBody.BillerCode === 0 ? '' : 'AND a.cBillerCode = @UserbillerCode'}
    `);

  const totalCount = result.recordset[0].TotalCount;
  return totalCount;
}

// Parameterized
module.exports.TotalOutofSyncBills = async (reqBody) => {
  const request = pool.request();
  const result = await request
    .input('UserBillerCode', sql.Int, reqBody.BillerCode)
    .query(`
      SELECT
        COUNT(a.id) as TotalCount
      FROM 
        [t_Bills] a
      LEFT JOIN [CustomerPortal].[dbo].[Bills] b 
        ON a.cBankRef = b.Atmref
      WHERE
        a.cBillDate != b.BillingMonth AND
        a.cBillDate = CONVERT(varchar(7), GETDATE(), 111) AND
        a.cModifiedDate = GETDATE()
        ${reqBody.BillerCode === 0 ? '' : 'AND a.cBillerCode = @UserBillerCode'}
    `);

  const totalCount = result.recordset[0].TotalCount;
  return totalCount;
}

// Parameterized
module.exports.BillingTable = async (reqBody) => {
  const request = pool.request();
  const query = `
     WITH RankedData AS (
         SELECT
             Id,
             cBillNum,
             cBankRef,
             cBillDate,
             nBillAmnt,
             cDuedate,
             cBillPeriod,
             nCurrentBal,
             withDueDate,
             cDateCreated,
             cCreatedBy,
             cBillerCode,
             nStatus,
             cInsertDate,
             cModifiedDate,
             cDisconnectionDate,
             nAccountStatus
         FROM [T_Bills]
         WHERE 
         [cBillDate] = @BillDate
         ${reqBody.BillerCode === 0 ? 'AND cBillerCode = @FilterBillerCode' : 'AND cBillerCode = @BillerCode'}
     )
     SELECT 
         *
     FROM RankedData
     ORDER BY [cDateCreated]
  DESC
  `;
 
  const result = await request
     .input('BillDate', sql.NVarChar, reqBody.BillDate)
     .input('BillerCode', sql.Int, reqBody.BillerCode)
     .input('FilterBillerCode', sql.Int, reqBody.FilterBillerCode)
     .query(query);
  return result.recordset;
}

// Parameterized
module.exports.BillerList = async () => {
  const getBillerListQuery = `
     SELECT 
       b.[BillerCode], COUNT(t.[cBankRef]) AS [TotalAccounts]
     FROM 
       [PAVI_Bills].[dbo].[Biller] b
     LEFT JOIN 
       t_Bills t ON b.[BillerCode] = t.[cBillerCode]
     WHERE 
       b.[IsActive] = @IsActive
     AND (
       b.[Name] NOT LIKE @ExcludedBillerName1
       AND b.[Name] NOT LIKE @ExcludedBillerName2
       AND b.[Name] NOT LIKE @ExcludedBillerName3
     )
     GROUP BY 
       b.[BillerCode]
     ORDER BY 
       b.[BillerCode];
  `;

  const request = pool.request();

  const isActive = 1;
  const excludedBillerName1 = '%Streamtech%';
  const excludedBillerName2 = '%Bria%';
  const excludedBillerName3 = '%Dummy%';

  request.input('IsActive', sql.Int, isActive);
  request.input('ExcludedBillerName1', sql.VarChar, excludedBillerName1);
  request.input('ExcludedBillerName2', sql.VarChar, excludedBillerName2);
  request.input('ExcludedBillerName3', sql.VarChar, excludedBillerName3);

  const result = await request.query(getBillerListQuery);
  return result.recordset;
}

// Parameterized
module.exports.CustomerInfo = async (reqBody) => {
  const request = pool.request();
 
  const customerInfo = `
     SELECT 
       [Atmref]
       ,[CustomerName]
       ,[AccountNumber]
       ,[Street]
       ,[City]
       ,[AccountStatus]
     FROM 
       [PAVI_Bills].[dbo].[Customers]
     WHERE LEFT
       ([AtmRef], 10) = @AtmRef
  `;
 
  const result = await request
     .input('AtmRef', reqBody.AtmRef)
     .query(customerInfo);
 
  return result.recordset;
}

// Parameterized - Stored Procedure
module.exports.ExecuteSyncingUpdate = async () => {
  const request = pool.request();
  const result = await request
     .execute('SyncingUpdate');
  return result.recordset;
 };