const sql = require("mssql/msnodesqlv8");
 
const customer_portalconfig = {
    database: 'CustomerPortal',
    server: 'EPRIME-JHON\\CONSODASHBOARD', 
    driver: 'msnodesqlv8',
    requestTimeout: 300000,
    options : {
        encrypt: false,
        trustServerCertificate: true,
        trustedConnection: true
        }
};

const customerportalpool = new sql.ConnectionPool(customer_portalconfig);

customerportalpool.connect().then(() => {
  return customerportalpool;
}).catch((err) => {
  console.error('Error connecting to the Customer Portal Database:', err);
});

// Parameterized
module.exports.BillingHistory = async (reqBody) => {
  const request = customerportalpool.request();
  const query = `
     WITH RankedData AS (
       SELECT       
         P.[Atmref]
         ,P.[PaymentDate]
         ,P.[BillNumber]
         ,P.[BillingMonth]
         ,P.[BillingPeriod]
         ,P.[TotalCharge]
         ,P.[Usage]
         ,P.[DueDate]
         ,P.[DisconDate]
         ,P.[DateCreated]
         ,P.[IsPaid]
         ,P.[PaymentMode]
       FROM [CustomerPortal].[dbo].[Bills] AS P
       WHERE [Atmref] = @AtmRef
       )
       SELECT
           *
       FROM RankedData
       ORDER BY PaymentDate DESC;
     `;
 
  const result = await request
     .input('AtmRef', sql.VarChar(20), reqBody.AtmRef) 
     .query(query);
 
  return result.recordset;
 };