const sql = require('mssql/msnodesqlv8');

const billspaymentConfig = {
    database: 'BillsPaymentDb',
    server: 'EPRIME-JHON\\CONSODASHBOARD',
    driver: 'msnodesqlv8',
    requestTimeout: 300000,
    options: {
        encrypt: false,
        trustServerCertificate: true,
        trustedConnection: true
    }
};

const billspaymentPool = new sql.ConnectionPool(billspaymentConfig);

billspaymentPool.connect().then(() => {
    console.log('Connected to the database');
}).catch((err) => {
    console.error('Error connecting to the Bills Payment Database:', err);
});

// Parameterized
module.exports.TotalPayment = async (reqBody) => {
    const request = billspaymentPool.request();

    let sqlQuery = `
    SELECT COUNT(Id) AS TotalPayments 
    FROM Payments
    WHERE
        CASE
            WHEN @BillerCode = 0 THEN 1 -- Include all rows when BillerCode is 0
            WHEN BillerCode = @BillerCode THEN 1 -- Filter by BillerCode when it's not 0
            ELSE 0
        END = 1;
    `;

    const result = await request
        .input('BillerCode', sql.Int, reqBody.BillerCode)
        .query(sqlQuery) 

    return result.recordset[0].TotalPayments;
};

// Parameterized
module.exports.TotalPaymentToday = async (reqBody) => {
    const request = billspaymentPool.request();

    let sqlQuery = `
    SELECT COUNT(Id) AS TotalPayments 
    FROM Payments
    WHERE
        CASE
            WHEN @BillerCode = 0 AND CONVERT(DATE, PaymentDate) = CAST(GETDATE() AS DATE) THEN 1
            WHEN BillerCode = @BillerCode AND CONVERT(DATE, PaymentDate) = CAST(GETDATE() AS DATE) THEN 1
            ELSE 0
        END = 1;  
    `;

    const result = await request
        .input('BillerCode', sql.Int, reqBody.BillerCode)
        .query(sqlQuery)

    return result.recordset[0].TotalPayments;
};

// Parameterized
module.exports.PaymentsTable = async (reqBody) => {
    const request = billspaymentPool.request();
    const query = `
        WITH RankedData AS (
            SELECT
                P.Id,
                P.AtmRef,
                P.AccountNo,
                P.Billnumber,
                P.Duedate,
                P.Payment,
                P.PaymentDate,
                P.BillerCode,
                P.PayMonggoSource,
                PS.StatusName,
                PC.Name,
                P.TxnRef,
                ROW_NUMBER() OVER (ORDER BY P.PaymentDate DESC) AS RowNum
            FROM [BillsPaymentDb].[dbo].[Payments] AS P
            JOIN [BillsPaymentDb].[dbo].[PaymentStatus] AS PS
                ON P.StatusId = PS.StatusId
            JOIN [BillsPaymentDb].[dbo].[PaymentCenter] AS PC
                ON P.PaymentCenterId = PC.PaymentCenterId
            WHERE CONVERT(DATE, P.PaymentDate) >= @startDate
            AND CONVERT(DATE, P.PaymentDate) <= @endDate
            ${reqBody.BillerCode === 0 ? '' : 'AND P.BillerCode = @BillerCode'}
        )
        SELECT
            *
        FROM RankedData
        ORDER BY PaymentDate DESC;
    `;

    const result = await request
        .input('startDate', reqBody.startDate)
        .input('endDate', reqBody.endDate)
        .input('BillerCode', reqBody.BillerCode)
        .query(query);

    console.log(result.recordset)

    return result.recordset;
};

// Parameterized
module.exports.PaymentHistory = async (reqBody) => {
    const request = billspaymentPool.request();
    const query = `
        WITH RankedData AS (
            SELECT
                P.[AtmRef]
                ,P.[Duedate]
                ,P.[Payment]
                ,P.[PaymentDate]
                ,P.[PaymentRef]
                ,P.[TxnRef]
                ,P.[PayMonggoSource]
                ,P.[BillerCode]
            FROM [BillsPaymentDb].[dbo].[Payments] AS P
            WHERE LEFT(P.[AtmRef], 10) = @AtmRef
        )
        SELECT
            *
        FROM RankedData
        ORDER BY PaymentDate DESC;
    `;

    const result = await request
        .input('AtmRef', sql.VarChar(10), reqBody.AtmRef)
        .query(query);

    return result.recordset;
};

