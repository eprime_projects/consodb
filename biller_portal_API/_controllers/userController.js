const express = require('express');
const cors = require("cors");

const bcrypt = require('bcrypt');
const auth = require('../auth')

const bodyParser = require('body-parser');
const sql = require("mssql/msnodesqlv8");

const app = express();

app.use(cors());
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded({extended: true}));
 
const config = {
    // user: 'dev_jhon',
    // password: 'Abc123456',
    //server: '10.15.226.112\\SQLSVRCONSOSTG',
    server: 'EPRIME-JHON\\CONSODASHBOARD',
    database: 'Dashboard',   
    driver: 'msnodesqlv8',
    options : {
        encrypt: false,
        trustServerCertificate: true,
        trustedConnection: true
        }
};
const pool = new sql.ConnectionPool(config);

pool.connect().then(() => {
  return true;
}).catch((err) => {
  console.error('Error connecting to the database:', err);
});


// Parameterized
module.exports.CheckEmail = async (reqBody) => {
    const request = pool.request();
    const checkEmailResult = await request
        .input('Email', sql.NVarChar, reqBody.Email)
        .query('SELECT email FROM Users WHERE email = @Email');

    if (checkEmailResult.recordset.length > 0) {
        return true;
    } else {
        return false;
    }
};

// Parameterized
module.exports.RegisterUser = async (reqBody) => {
    const saltRounds = 10;
    const Password = await bcrypt.hash(reqBody.Password, saltRounds);

    const request = pool.request();
    const checkEmailResult = await request
        .input('checkEmail', sql.NVarChar, reqBody.Email)
        .query('SELECT Email FROM Users WHERE Email = @checkEmail');

    if (checkEmailResult.recordset.length > 0) {
        return false; 
    }

    const insertResult = await request
        .input('FirstName', sql.NVarChar, reqBody.FirstName)
        .input('LastName', sql.NVarChar, reqBody.LastName)
        .input('Email', sql.NVarChar, reqBody.Email)
        .input('Password', sql.NVarChar, Password) 
        .input('Role', sql.Int, 4)
        .input('BillerCode', sql.Int, reqBody.BillerCode)
        .input('Status', sql.Int, 1)
        .input('ApprovedBy', sql.NVarChar, " ")
        .query('INSERT INTO Users (FirstName, LastName, Email, Password, Role, BillerCode, Status, ApprovedBy) VALUES (@FirstName, @LastName, @Email, @Password, @Role, @BillerCode, @Status, @ApprovedBy)');
    
    if (insertResult.rowsAffected[0] > 0) {
        return true;
    } else {
        return false;
    }
};

// Parameterized
module.exports.LoginUser = async (reqBody) => {
    const request = pool.request();
    request.input('Email', sql.NVarChar, reqBody.Email)

    const query = `
        SELECT Id, FirstName, LastName, Email, Password, BillerCode, Status
        FROM Users
        WHERE Email = @Email
    `;

    const result = await request.query(query);

    if (!result.recordset || result.recordset.length === 0) {
        return false;
    }

    const user = result.recordset[0];

    if (!user.Password || user.Status !== 2) {
        return false;
    }

    const passwordMatch = await bcrypt.compare(reqBody.Password, user.Password);
    if (passwordMatch) {
        await insertLoginTime(user.Id, user.Email);

        const accessToken = auth.createAccessToken(user);
        return { access: accessToken };
    } else {
        return false;
    }
};

const insertLoginTime = async (userId, userEmail) => {
    const request = pool.request();
    request.input('UserId', sql.Int, userId);
    request.input('Email', sql.NVarChar, userEmail);

    const insertQuery = `
        INSERT INTO UsersLoginLog (UserId, Email, LastActive)
        VALUES (@UserId, @Email, GETDATE())
    `;

    await request.query(insertQuery);
};

// Parameterized
module.exports.UserData = async (reqBody) => {
    const request = pool.request();
    request.input('Id', sql.Int, reqBody.Id);

    const query = `
        SELECT Id, FirstName, LastName, Email, '*****' AS Password, BillerCode, Role, Status
        FROM Users
        WHERE Id = @Id
    `;

    const result = await request.query(query);

    if (result.recordset.length === 0) {
        return null;
    } else {
        return result.recordset[0];
    }
};

// Parameterized
module.exports.AllUsers = async (reqBody) => {
    const request = pool.request();
    
    let sqlQuery = `
    SELECT Id, FirstName, LastName, Email, '*****' AS Password, BillerCode, Role, Status, ApprovedBy 
    FROM Users
    `;

    if (reqBody.BillerCode !== 0) {
        sqlQuery += ` WHERE BillerCode = @BillerCode`;
        request.input('BillerCode', sql.Int, reqBody.BillerCode);
    }

    const result = await request.query(sqlQuery);

    if (result.recordset.length !== 0) {
        const users = result.recordset;
        return users;
    } else {
        return false;
    }
};

// Parameterized
module.exports.UpdateInformation = async (reqBody) => {
    const request = pool.request();

    console.log('Updating information for user with ID:', reqBody.Id);
    console.log('NewStatus:', reqBody.NewStatus);
    console.log('NewRole:', reqBody.NewRole);
    console.log('ApprovedBy:', reqBody.ApprovedBy);

    let query = `
    UPDATE Users
    SET 
        Status = CASE 
                    WHEN @NewStatus IS NOT NULL THEN @NewStatus
                    ELSE Status 
                END,
        Role = CASE WHEN @NewRole IS NOT NULL THEN @NewRole ELSE Role END,
        ApprovedBy = CASE 
                        WHEN @NewStatus = 2 AND Status = 1 THEN @ApprovedBy 
                        WHEN @NewStatus = 2 AND Status = 3 THEN @ApprovedBy -- Update ApprovedBy when changing from 3 to 2
                        WHEN @NewStatus IN (1, 3) THEN ' ' -- Set ApprovedBy to a white space for Status 1 or 3
                        ELSE ApprovedBy 
                    END
    WHERE Id = @Id;
    
    UPDATE Users
    SET 
        Status = CASE 
                    WHEN @SecondaryStatus IS NOT NULL THEN @SecondaryStatus 
                    ELSE Status 
                 END
    WHERE Id = @Id AND @NewStatus = 2;
    `;

    const result = await request
        .input('Id', sql.Int, reqBody.Id)
        .input('NewStatus', sql.Int, reqBody.NewStatus)
        .input('NewRole', sql.Int, reqBody.NewRole)
        .input('ApprovedBy', sql.NVarChar, reqBody.ApprovedBy)
        .input('SecondaryStatus', sql.Int, reqBody.SecondaryStatus)
        .query(query);
        

    if (result.rowsAffected && result.rowsAffected[0] > 0) {
        return true;
    } else {
        return false;
    }
}

// Parameterized
module.exports.UpdatePassword = async (reqBody) => {
    const saltRounds = 10;
        const Password = await bcrypt.hash(reqBody.Password, saltRounds);

        const request = pool.request();
        const result = await request
            .input('Password', sql.NVarChar, Password)
            .input('Id', sql.Int, reqBody.Id)
            .query(`UPDATE Users
                    SET Password = @Password
                    WHERE Id = @Id;`);

        if (result.rowsAffected[0] > 0) {
            return true;
        } else {
            return false;
        }
};



    