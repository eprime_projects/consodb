const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const sql = require('mssql/msnodesqlv8');
const app = express();

app.use(cors());
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

const port = process.env.PORT || 5000;
const config = {
    // user: 'dev_jhon',
    // password: 'Abc123456',
    // server: '10.15.226.112\\SQLSVRCONSOSTG', 
    server: 'EPRIME-JHON\\CONSODASHBOARD',   
    driver: 'msnodesqlv8',
    options : {
        encrypt: false,
        trustServerCertificate: true,
        trustedConnection: true
        }
    };

const pool = new sql.ConnectionPool(config);

pool.connect().then(() => {
  return pool;
}).catch((err) => {
  console.error('Error connecting to the database:', err);
});

const userRoute = require('./_routes/userRoute');
const paymentRoute = require('./_routes/paymentRoute')
const pavi_billsRoute = require('./_routes/pavi_billsRoute')
const customer_portal = require('./_routes/customer_portalRoute')

app.use('/users', userRoute);
app.use('/billspayment', paymentRoute);
app.use('/pavibills', pavi_billsRoute);
app.use('/customerportal', customer_portal)

app.listen(port, () => {
  console.log(`Now connected to port ${port}`);
});