const express = require('express');
const router = express.Router();
const pavi_billsController = require('../_controllers/pavi_billsController');
const cors = require('cors');
const app = express();
app.use(cors());

router.post("/synchedbills", async (req, res) => {
    pavi_billsController
    .TotalSynchedBills(req.body)
    .then(resultFromController => {res.send(resultFromController.toString());
    })
    });

router.post("/outofsyncbills", async (req, res) => {
    pavi_billsController
    .TotalOutofSyncBills(req.body)
    .then(resultFromController => {res.send(resultFromController.toString());
    })
    });

router.post("/billingtable", (req, res) => {
    pavi_billsController
        .BillingTable(req.body)
        .then(resultFromController =>  res.send(resultFromController));
});

router.get("/billerlist", (req, res) => {
    pavi_billsController
        .BillerList()
        .then(resultFromController =>  res.send(resultFromController));
});

router.post("/customerinfo", (req, res) => {
    pavi_billsController
        .CustomerInfo(req.body)
        .then(resultFromController =>  res.send(resultFromController));
});

router.get("/synchingupdate", (req, res) => {
    pavi_billsController
        .ExecuteSyncingUpdate()
        .then(resultFromController =>  res.send(resultFromController));
});
    

module.exports = router;
