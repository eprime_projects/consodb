const express = require('express');
const router = express.Router();
const userController = require('../_controllers/userController');
const auth = require('../auth')

router.post("/checkEmail", (req, res) => {
    userController
    .CheckEmail(req.body)
    .then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
    userController
    .RegisterUser(req.body)
    .then(resultFromController => res.send(resultFromController))
});

router.post('/login', (req, res) => {
    userController
    .LoginUser(req.body)
    .then(resultFromController => res.send(resultFromController));
});

router.get("/userdetails", auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController
    .UserData({Id: userData.Id})
    .then(resultFromController => res.send(resultFromController))
});

router.post("/allusers", (req, res) => {
    userController
    .AllUsers(req.body)
    .then(resultFromController => res.send(resultFromController))
})

router.post("/updateuserinfo", (req, res) => {
    userController
    .UpdateInformation(req.body)
    .then(resultFromController => res.send(resultFromController))
});

router.post("/updatepassword", (req, res) => {
    userController
    .UpdatePassword(req.body)
    .then(resultFromController => res.send(resultFromController))
});



module.exports = router;


