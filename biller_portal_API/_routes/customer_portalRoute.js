const express = require('express');
const router = express.Router();
const customer_portal = require('../_controllers/customer_portalController');
const cors = require('cors');
const app = express();
app.use(cors());

router.post("/customerbillinghistory", (req, res) => {
    customer_portal    
        .BillingHistory(req.body)
        .then(resultFromController =>  res.send(resultFromController));
});

module.exports = router;
