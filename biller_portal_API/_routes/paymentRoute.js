const express = require('express');
const router = express.Router();
const paymentController = require('../_controllers/paymentController');
const auth = require('../auth')
const cors = require('cors');
const app = express();
app.use(cors());

router.post("/totalpayment", (req, res) => {
    paymentController
    .TotalPayment(req.body)
    .then(resultFromController => {res.send(resultFromController.toString());
    })
});

router.post("/totalpaymenttoday", (req, res) => {
    paymentController
    .TotalPaymentToday(req.body)
    .then(resultFromController => {res.send(resultFromController.toString());
    })
});

router.post("/recentpayments", (req, res) => {
    paymentController
        .PaymentsTable(req.body)
        .then(resultFromController =>  res.send(resultFromController));
});

router.post("/paymenthistory", (req, res) => {
    paymentController
        .PaymentHistory(req.body)
        .then(resultFromController =>  res.send(resultFromController));
});


module.exports = router;
