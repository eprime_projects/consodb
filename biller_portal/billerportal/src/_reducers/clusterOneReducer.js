const initialState = {
    clusterOneData: [],
 };
 
 const clusterOneReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_CLUSTER_ONE_DATA":
            return {
                ...state,
                clusterOneData: action.payload,
            };
        default:
            return state;
    }
 };
 
 export default clusterOneReducer;