import { FETCH_TOTAL_PAYMENT_REQUEST, SET_TOTAL_PAYMENT, FETCH_TOTAL_PAYMENT_TODAY_REQUEST, SET_TOTAL_PAYMENT_TODAY, FETCH_SYNCHED_BILLS_REQUEST, SET_SYNCHED_BILLS, FETCH_OUT_OF_SYNCHED_BILLS_REQUEST, SET_OUT_OF_SYNCHED_BILLS } from "../_actions/dashboardActions";
const initialDashboardBillerListState = {
    dashboardBillerListData: [],
};

export const dashboardBillerListReducer = (state = initialDashboardBillerListState, action) => {
    switch (action.type) {
        case "BILLER_LIST_DATA":
            return {
                ...state,
                dashboardBillerListData: action.payload,
            };
        default:
            return state;
    }
};

const initialTotalPaymentState = {
    totalPayment: [],
    loading: false,
};

export const totalPaymentReducer = (state = initialTotalPaymentState, action) => {
    switch (action.type) {
        case FETCH_TOTAL_PAYMENT_REQUEST:
            return {
                ...state,
                loading: true
            };
        case SET_TOTAL_PAYMENT:
            return {
                ...state,
                totalPayment: action.payload,
                loading: false
            };
        default:
            return state;
    }
};

const initialTotalPaymentTodayState = {
    totalPaymentToday: [],
    loading: false
};

export const totalPaymentTodayReducer = (state = initialTotalPaymentTodayState, action) => {
    switch (action.type) {
        case FETCH_TOTAL_PAYMENT_TODAY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case SET_TOTAL_PAYMENT_TODAY:
            return {
                ...state,
                totalPaymentToday: action.payload,
                loading: false
            };
        default:
            return state
    }
};

const initialSynchedBillsState = {
    synchedBills: [],
    loading: false
};

export const synchedBillsReducer = (state = initialSynchedBillsState, action) => {
    switch (action.type) {
        case FETCH_SYNCHED_BILLS_REQUEST:
            return {
                ...state,
                loading: true
            };
        case SET_SYNCHED_BILLS: 
            return {
                ...state,
                synchedBills: action.payload,
                loading: false
            };
        default:
            return state
    }
};

const initialOutOfSynchedBills = {
    outOfSyncedBills: [],
    loading: false
};

export const outOfSynchedBillsReducers = (state = initialOutOfSynchedBills, action) => {
    switch (action.type) {
        case FETCH_OUT_OF_SYNCHED_BILLS_REQUEST:
            return {
                ...state,
                loading: true 
            };
        case SET_OUT_OF_SYNCHED_BILLS:
            return {
                ...state,
                outOfSynchedBills: action.payload,
                loading: false
            }
        default: 
            return state
    }
}; 