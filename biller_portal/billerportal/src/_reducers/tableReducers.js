import { FETCH_PAYMENT_TABLE_REQUEST, SET_PAYMENT_TABLE, FETCH_USER_TABLE_REQUEST, SET_USER_TABLE, SET_START_DATE, SET_END_DATE, SET_BILLER_CODE } from "../_actions/tablesActions";

const initialPaymentDatesState = {
    startDate: new Date(),
    endDate: new Date(),
    billerCode: null,
  };
  
  export const paymentDatesReducers = (state = initialPaymentDatesState, action) => {
    switch (action.type) {
      case 'SET_START_DATE':
        return {
          ...state,
          startDate: action.payload
        };
  
      case 'SET_END_DATE':
        return {
          ...state,
          endDate: action.payload
        };
  
      case 'SET_BILLER_CODE':
        return {
          ...state,
          billerCode: action.payload,
        };
  
      default:
        return state;
    }
  };
  
//   const initialBillerCodeState = {
//     billerCode: null,
//   }

//   export const billerCodeReducers = (state =initialBillerCodeState, action ) => {
//     switch (action.type) {
//         case FETCH_PAYMENT_TABLE_REQUEST:
//             return {
//                 ...state,
//                 loading: true
//             };
//         case SET_PAYMENT_TABLE: 
//             return {
//                 ...state,
//                 paymentTableData: action.payload,
//                 loading: false
//             }
//         default:
//             return state;
//     }
//   }


const initialPaymentTableState = {
    paymentTableData: [],
    loading: false
};

export const paymentTableReducers = (state = initialPaymentTableState, action) => {
    switch (action.type) {
        case FETCH_PAYMENT_TABLE_REQUEST:
            return {
                ...state,
                loading: true
            };
        case SET_PAYMENT_TABLE: 
            return {
                ...state,
                paymentTableData: action.payload,
                loading: false
            }
        default:
            return state;
    }
};

const initialUserTableState = {
    userTableData: [],
    loading: false
};

export const userTableReducers = (state = initialUserTableState, action) => {
    switch (action.type) {
        case FETCH_USER_TABLE_REQUEST:
            return {
                ...state,
                loading: true
            };
        case SET_USER_TABLE: 
            return {
                ...state,
                userTableData: action.payload,
                loading: false
            }
        default:
            return state;
    }
};

