import { configureStore } from "@reduxjs/toolkit";

import sidebarReducer from "./_actions/sidebarActions";
import clusterOneReducer from "./_reducers/clusterOneReducer";
import { dashboardBillerListReducer, totalPaymentReducer, totalPaymentTodayReducer, synchedBillsReducer, outOfSynchedBillsReducers } from "./_reducers/dashboardReducer";
import { paymentTableReducers, userTableReducers, paymentDatesReducers } from "./_reducers/tableReducers";

const defaultMiddlewareConfig = {
    serializableCheck: {
        ignoredPaths: ["payload"],
    }
  };
  
  export default () => {
    const store = configureStore({
      reducer: {
        sidebar: sidebarReducer,
        clusterOne: clusterOneReducer,
        dashboardBillerList: dashboardBillerListReducer,
        totalPayment: totalPaymentReducer,
        totalPaymentToday: totalPaymentTodayReducer,
        synchedBills: synchedBillsReducer,
        outOfSynchedBills: outOfSynchedBillsReducers,
        paymentTableStore: paymentTableReducers,
        userTableStore: userTableReducers,
        paymentDateStore :paymentDatesReducers,
      },
      middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware(defaultMiddlewareConfig),
    });
    return store;
  };