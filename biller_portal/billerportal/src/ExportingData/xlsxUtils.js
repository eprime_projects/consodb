import * as XLSX from 'xlsx';

export function convertArrayOfObjectsToXLSX(array) {
  const ws = XLSX.utils.json_to_sheet(array);
  const wb = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  const buf = XLSX.write(wb, { bookType: 'xlsx', type: 'buffer' });
  return buf;
}

export function downloadXLSX(array) {
  const blob = new Blob([convertArrayOfObjectsToXLSX(array)], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  
  const link = document.createElement('a');
  const filename = 'billing_data.xlsx';

  link.href = URL.createObjectURL(blob);
  link.download = filename;
  link.click();
}
