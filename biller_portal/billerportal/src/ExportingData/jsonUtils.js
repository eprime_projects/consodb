export function downloadJSON(data) {
    const json = JSON.stringify(data, null, 2); // Convert the data to a nicely formatted JSON string
    const blob = new Blob([json], { type: 'application/json' });
  
    const link = document.createElement('a');
    const filename = 'data.json';
  
    link.href = URL.createObjectURL(blob);
    link.download = filename;
    link.click();
  }
  