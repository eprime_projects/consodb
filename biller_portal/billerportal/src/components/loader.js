import './styles/loader.css'
import { Box } from '@chakra-ui/react'

export function Loader(){
    return(
        <>
        <Box className="spinnerContainer" mt='150px'>
            <div className="spinner"></div>
            <div className="loader">
                <p>loading</p>
                <div className="words">
                <span className="word">database</span>
                <span className="word">syntax query</span>
                <span className="word">information</span>
                <span className="word">database</span>
                <span className="word">information</span>
            </div>
            </div>
        </Box>
        </>
    )
}

export function Loader1 (){
    return(
        <>
            <Box className="spinnerContainer" mt='60px'>
                <div className="spinner"></div>
            </Box>        
        </>
    )
}