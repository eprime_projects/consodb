export const userRoles = [
        { value: 1, label: 'DevOps' },
        { value: 2, label: 'Eprime Admin' },
        { value: 3, label: 'Branch Admin' },
        { value: 4, label: 'User' },
      ];

export const userStatus = [
          { value: 1, label: 'Pending' },
          { value: 2, label: 'Active' },
          { value: 3, label: 'Rejected/Removed' },
        ];
        