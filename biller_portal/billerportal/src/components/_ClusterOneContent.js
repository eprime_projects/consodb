import React, { useEffect } from 'react';
import DataTable from 'react-data-table-component';
import { connect } from 'react-redux';
import { fetchClusterOneData, setClusterOneData } from '../_actions/clusterOneActions';
import { Box, Text, Image, Grid, GridItem, HStack } from '@chakra-ui/react';
import { Loader } from "./loader";
import tableicon from '../images/tableicon.png';

function ClusterOneContent({ clusterOneData, fetchClusterOneData }) {

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetchClusterOneData();
      setClusterOneData(data);
    };

    fetchData();
  }, [fetchClusterOneData]);

  const columns = [
    { name: 'Branch', selector: (row) => row.Name },
    { name: 'No. of Updated Accounts', selector: (row) => row.Updated },
    { name: 'No. of Not Updated Accounts', selector: (row) => row['Not Updated'] },
  ];

  const data = clusterOneData.map((clusterOneListData) => ({
    Name: clusterOneListData.Name,
    Updated: clusterOneListData.Updated ?? 0,
    'Not Updated': clusterOneListData['Not Updated'] ?? 0,
  }));

  return (
    <>
      <Grid templateColumns={{ base: 'repeat(8, 1fr)', xl: 'repeat(6, 1fr)' }} templateRows="repeat(3, 1fr)" gap={4}>
        <GridItem rowSpan={3} colSpan={{ base: '8', xl: '4' }}>
          <Box
            w="100%"
            h="750px"
            overflowY="auto"
            border="1px solid #ccc"
            borderRadius="5px"
            className="custom-scroll billerListTable"
            px="20px"
            py="10px"
          >
            <HStack>
              <Image src={tableicon} style={{ width: '50px' }} />
              <Text fontSize="30px">Cluster One</Text>
            </HStack>

            <DataTable
              columns={columns}
              data={data}
              fixedHeader
              pointerOnHover={true}
              highlightOnHover={true}
              noDataComponent={<Loader />}
              className="custom-scroll"
            />
          </Box>
        </GridItem>
      </Grid>
    </>
  );
}

const mapStateToProps = (state) => ({
  clusterOneData: state.clusterOne.clusterOneData,
});

const mapDispatchToProps = {
  fetchClusterOneData,
};

export default connect(mapStateToProps, mapDispatchToProps)(ClusterOneContent);
