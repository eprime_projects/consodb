import { useState, useEffect, useCallback, useContext } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'; 
import { downloadCSV } from '../ExportingData/csvUtils';
import { downloadXLSX } from "../ExportingData/xlsxUtils";
import { downloadJSON } from "../ExportingData/jsonUtils";
import { SearchIcon } from "@chakra-ui/icons";
import { FcFilledFilter } from 'react-icons/fc'
import { billerCodeOptions } from './billerList'; 
import {Loader} from "./loader";
import UserContext from "../UserContext";
import '../App.css'


import { 
    Box, 
    Text, 
    Button, 
    Input, 
    Checkbox, 
    InputGroup, 
    InputLeftElement, 
    Menu, 
    MenuItem, 
    MenuList,
    Select,
    Grid, 
    GridItem,
    Tooltip,   
    MenuButton
} from "@chakra-ui/react";
import {format} from "date-fns";

export default function PaymentsContent() {
    const {user} = useContext(UserContext);

    const [paymentTableData, setPaymentTable] = useState([]);
    const [loading, setLoading] = useState(false);
    const [selectedRows, setSelectedRows] = useState([]); 
    const [selectAll, setSelectAll] = useState(false);
    const [selectedBillerCode, setSelectedBillerCode] = useState("");
    const [searchQuery, setSearchQuery] = useState("");
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    useEffect(() => {
        setEndDate(new Date());
    }, []);

    useEffect(() => {
        const fetchPaymentDB = async () => {
          try {
            setLoading(true);
            if (startDate && endDate) {
                const response = await fetch(`${process.env.REACT_APP_API_URL}/billspayment/recentpayments`, {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      startDate: format(startDate, 'yyyy-MM-dd'),
                      endDate: format(endDate, 'yyyy-MM-dd'),
                      BillerCode: user.BillerCode,
                    }),
                  });
          
                  const data = await response.json();
                  setPaymentTable(data);
                  setLoading(false);
            }
          } catch (error) {
            console.error('Error fetching payment data:', error);
          }
        };
    
        fetchPaymentDB();
      }, [startDate, endDate, user.BillerCode]);
    

      const onChange = (dates) => {
        const [start, end] = dates;  
        setStartDate(start);
        setEndDate(end);
    };
    
    const handleRowSelected = (selectedRowIds) => {
        if (selectedRowIds.length === 0) {
            setSelectAll(false);
        } else if (selectedRowIds.length === filteredData.length) {
            setSelectAll(true);
        }
        setSelectedRows(selectedRowIds);
    }
    
    const handleExportXLSX = () => {
        const rowsToExport = selectAll ? data : selectedRows;
        downloadXLSX(rowsToExport);
    };
    const handleExportCSV = () => {
        const rowsToExport = selectAll ? data : selectedRows;
        downloadCSV(rowsToExport);
    };
    const handleExportJSON = () => {
        const rowsToExport = selectAll ? data : selectedRows;
        downloadJSON(rowsToExport);
    };
    
    // Modify Column Step 1
    const data = paymentTableData.map((billingItem) => ({
        id: billingItem.Id,
        billerCode: billingItem.BillerCode,
        atmRef: billingItem.AtmRef,
        accountNo: billingItem.AccountNo,
        billNo: billingItem.Billnumber,
        dueDate: billingItem.Duedate,
        payment: billingItem.Payment,
        PaymentCenter: billingItem.Name,
        TxnRef: billingItem.TxnRef,
        statusName: billingItem.StatusName,
        paymentDate: billingItem.PaymentDate, 
        payMongo: billingItem.PayMonggoSource  
    }));
    
    // Modify Column Step 2
    const initialColumns  = [
        {
            name: 'Id',
            selector: row => row.id,
            sortable: true,
            width: '6%',            
            style: {
                color: '#202124',
                fontSize: '14px',
                fontWeight: 500,
            },
            visible: user.Role === 1,
        },
        {
            name: 'Biller',
            selector: row => row.billerCode,
            sortable: true,
            width: '5%',            
            visible: user.Role === 1 ||  user.Role === 2,
        },
        {
            name: 'ATM Ref',
            selector: row => row.atmRef,
            sortable: true,
            visible: true,
            width: '10%',            

        },
        {
            name: 'Account No.',
            sortable: true,
            visible: true,
            width: '10%',            
            cell: (row) => (
                <>
                  <div>
                    <div style={{color: '#822727', backgroundColor: '#fed7d7', padding: '5px 10px 5px 10px', borderRadius: '25px', minWidth: '120px', display: 'flex', justifyContent: 'center' }}>
                      {row.accountNo}
                    </div>
                  </div>
                </>
              ),
        },
        {
            name: 'Bill No.',
            selector: row => row.billNo,
            sortable: true,
            visible: true,
        },
        {
            name: 'Due Date',
            selector: row => row.dueDate,
            sortable: true,
            visible: true,
        },
        {
            name: 'Status',
            selector: row => row.statusName,
            sortable: true,
            width: '7%',            
            visible: true,
        },
        {
            name: 'Payment',
            selector: row => row.payment,
            sortable: true,
            width: '8%',            
            visible: true,
        },
        {
            name: 'Payment Date',
            selector: row => row.paymentDate,
            sortable: true,
            visible: true,
        },
        {
            name: 'Payment Center',
            selector: row => row.PaymentCenter,
            sortable: true,
            visible: true,
        },
        {
            name: 'Txn Ref',
            selector: row => row.TxnRef,
            sortable: true,
            visible: true,
        },
        {
            name: 'Pay Mongo Source',
            selector: row => row.payMongo,
            sortable: true,
            visible: true,
        },
    ];
    
    const [selectedColumns, setSelectedColumns] = useState(initialColumns);
    
    const visibleColumns = selectedColumns.filter((column) => column.visible);
    
    const paginationOptions = {
        rowsPerPageText: "Rows per page:",
        rangeSeparatorText: "of",
        selectAllRowsItem: true,
        selectAllRowsItemText: "All",
    };

    const DataTableCustomStyles = {
        headRow: {
            style: {
                border: 'none',
            },
        },
        headCells: {
            style: {
                color: '#202124',
                fontSize: '14px',
            },
        },
        pagination: {
            style: {
                border: 'none',
            },
        }
    };
    
    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };
    
    // Column Filter - Display only the currently checked items
    const toggleColumnVisibility = (columnName) => {
        const updatedColumns = selectedColumns.map((column) => {
            if (column.name === columnName) {
                return { ...column, visible: !column.visible };
            }
            return column;
        });
        setSelectedColumns(updatedColumns);
    };
    
    const resetColumnVisibility = () => {
        const resetColumns = initialColumns.map((column) => ({
            ...column,
            visible: true,
        }));
        setSelectedColumns(resetColumns);
    };
    
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    
    const handleDropdownToggle = () => {
        setIsDropdownOpen(!isDropdownOpen);
    };
    
    const handleOkClick = () => {
        setIsDropdownOpen(false);
    };
    
    // Biller Code selection
    const handleBillerCodeSelect = (e) => {
      const selectedCode = e.target.value;
      setSelectedBillerCode(selectedCode);
    };
    
    const filteredData = data.filter((row) => {
    const selectedBillerCodeInt = parseInt(user.billerCode, 10);
    const isSelectedBillerCodeMatch = isNaN(selectedBillerCodeInt) || row.billerCode === selectedBillerCodeInt;
    const isOtherCriteriaMatch =
    (
    (isNaN(selectedBillerCodeInt) || row.billerCode === selectedBillerCodeInt) &&
    (row.id && row.id.toString().includes(searchQuery))
    ) ||
    row.atmRef?.includes(searchQuery) ||
    row.accountNo?.includes(searchQuery) ||
    row.billNo?.includes(searchQuery) ||
    (row.statusName?.toLowerCase().includes(searchQuery.toLowerCase())) ||
    row.dueDate?.includes(searchQuery) ||
    (row.payment && row.payment.toString().includes(searchQuery)) ||
    row.paymentDate?.includes(searchQuery) ||
    (row.payMongo?.toLowerCase().includes(searchQuery.toLowerCase()))
    return isSelectedBillerCodeMatch && isOtherCriteriaMatch;
    });

    const PaymentExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;


    function BillerCodeSection({ user, selectedBillerCode, handleBillerCodeSelect, billerCodeOptions }) {
        if (user.Role === 1 || user.Role === 2) {
            return (
                <GridItem colSpan={3}>
                    <Select
                        value={selectedBillerCode}
                        onChange={handleBillerCodeSelect}
                        border="1px"
                        borderRadius='10px'
                        borderColor="gray.400"
                    >
                        <option value="">Branches List</option>
                        {billerCodeOptions.map((biller) => (
                            <option key={biller.value} value={biller.value}>
                                {biller.label}
                            </option>
                        ))}
                    </Select>
                </GridItem>
            );
        } else {
            return null; 
        }
    }
    
    return (
        <>
        <Box>                
            <Text fontSize="2rem" fontWeight="bold">Payments</Text>
            <Grid templateColumns='repeat(12, 1fr)' templateRows='repeat(3, 1fr)' gap={2}  mt='1rem'>
                <GridItem colSpan={{base: '12', lg: '3', xl: '3'}}>
                    <InputGroup>
                        <InputLeftElement pointerEvents="none" children={<SearchIcon color="gray.700" />}/>
                        <Input
                        type="text"
                        placeholder="Search data from table..."
                        value={searchQuery}
                        onChange={handleSearchChange} 
                        border="1px"
                        borderRadius='10px'
                        borderColor="gray.400"
                        outline="none"
                        />
                    </InputGroup>
                    </GridItem>

                <GridItem colSpan={1} colStart={{base: '6', lg:'10'}}>
                    <Tooltip label="Must select a row" isDisabled={selectedRows.length > 0}>
                        <Button onClick={handleExportXLSX} cursor="pointer" isDisabled={selectedRows.length === 0}  border="1px"
                        borderColor="gray.400" borderRadius='10px'>
                            Export XLSX
                        </Button>
                    </Tooltip>
                </GridItem>


                <GridItem colSpan={1}>
                    <Tooltip label="Must select a row" isDisabled={selectedRows.length > 0}>
                        <Button onClick={handleExportCSV} cursor="pointer" isDisabled={selectedRows.length === 0}  border="1px"
                        borderColor="gray.400" borderRadius='10px'>
                            Export CSV
                        </Button>
                    </Tooltip>
                </GridItem>

                <GridItem colSpan={1}>
                    <Tooltip label="Must select a row" isDisabled={selectedRows.length > 0}>
                        <Button onClick={handleExportJSON}  cursor="pointer" isDisabled={selectedRows.length === 0}  border="1px"
                        borderColor="gray.400" borderRadius='10px'>
                            Export JSON
                        </Button>
                    </Tooltip>
                </GridItem>

                <GridItem colSpan={{base: '12', lg: '3', xl: '3'}}>
                    <BillerCodeSection
                    user={user}
                    selectedBillerCode={selectedBillerCode}
                    handleBillerCodeSelect={handleBillerCodeSelect}
                    billerCodeOptions={billerCodeOptions}
                    />
                </GridItem>
                
                <GridItem colSpan={{base: '6', lg: '2'}} colStart={{base: '0', lg: '10'}}>
                    <Box h='100%' border='1px' borderColor='gray.400' borderRadius='10px' alignItems='center' display='flex'>
                    <   DatePicker
                        selected={startDate}
                        onChange={onChange}
                        startDate={startDate}
                        endDate={endDate}
                        selectsRange
                        /> 
                    </Box>
                </GridItem>
                   
                <GridItem >    
                    <Menu isOpen={isDropdownOpen} >
                        <Tooltip label='Filter Columns'>
                            <MenuButton 
                            onClick={handleDropdownToggle} 
                            backgroundColor='white' 
                            borderRadius='10px'
                            border="1px" 
                            borderColor="gray.400"
                            height='100%'
                            width='40%'>                          
                                <Grid templateColumns='repeat(3, 1fr)' >
                                    <GridItem colSpan={1} colStart={2}>
                                        <FcFilledFilter/>
                                    </GridItem>
                                </Grid>
                            </MenuButton>
                        </Tooltip>
                        <MenuList>
                        {selectedColumns.map((column) => (
                            <MenuItem key={column.name}>
                                <Checkbox
                                isChecked={column.visible}
                                onChange={() => toggleColumnVisibility(column.name)}
                                >
                                {column.name}
                                </Checkbox>
                            </MenuItem>
                        ))}
                            <MenuItem>
                                <Grid templateColumns='repeat(5, 1fr)' >
                                    <GridItem colSpan={1} colStart={2}>
                                        <Text onClick={handleOkClick} className="buttonCol">
                                            Done
                                        </Text>
                                    </GridItem>
                                    <GridItem colSpan={1} colStart={4}>
                                        <Text onClick={resetColumnVisibility} className="buttonCol">
                                            Reset
                                        </Text>
                                    </GridItem>
                                </Grid>                               
                            </MenuItem>
                        </MenuList>
                    </Menu>
                </GridItem>
            </Grid>
            
            <DataTable
                columns={visibleColumns}
                data={filteredData}
                selectableRows
                onSelectedRowsChange={handleRowSelected}
                selectableRowProps={selectAll ? { checked: true } : null}                
                expandableRows
                expandableRowsHideExpander
                expandOnRowClicked
                expandableRowsComponent={PaymentExpandedComponent}
                pagination={true}
                paginationPerPage={10}
                paginationRowsPerPageOptions={[10, 20, 30]}
                paginationTotalRows={filteredData.length}
                paginationServer={false}
                paginationOptions={paginationOptions}
                pointerOnHover={true}
                highlightOnHover={true}
                customStyles={DataTableCustomStyles}
                progressPending={loading}
                progressComponent={<Loader/>}
            />
        </Box>
        </>
    )
}