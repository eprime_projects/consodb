import { useState, useEffect, useCallback, useContext } from "react";
import UserContext from "../UserContext";

import DataTable from 'react-data-table-component';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'; 

import { downloadCSV } from '../ExportingData/csvUtils';
import { downloadXLSX } from "../ExportingData/xlsxUtils";
import { downloadJSON } from "../ExportingData/jsonUtils";
import { SearchIcon } from "@chakra-ui/icons";
import { FcFilledFilter } from 'react-icons/fc'
import { billerCodeOptions } from './billerList'; 

import {Loader} from "./loader";
import NoResultsFound from "./noResults";

import '../App.css'

import {
    // Chakra UI components for basic UI elements
    Box,
    Text,
    Button,
    Input,
    Checkbox,
    InputGroup,
    InputLeftElement,
    Select,
    Grid,
    GridItem,
    Tooltip,
    Menu,
    MenuItem,
    MenuList,
    MenuButton,
    Badge,
  
    // Chakra UI components for modals
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
  } from "@chakra-ui/react";
  

export default function BillingsContent() {
    const { user } = useContext(UserContext);
  
    const [billingInfo, setBillingInfo] = useState([]);
    const [loading, setLoading] = useState(false);
    const [startDate, setStartDate] = useState(new Date());
    const [previousDate, setPreviousDate] = useState(startDate);
    const [selectedBillerCode, setSelectedBillerCode] = useState('');

    const fetchPaymentDB = useCallback(async () => {
      setLoading(true);
        const startDateString = startDate.toLocaleDateString('en-US', { year: 'numeric', month: '2-digit' }).split('/').reverse().join('/');
  
        await fetch(`${process.env.REACT_APP_API_URL}/pavibills/billingtable`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            BillDate: startDateString,
            BillerCode: user.BillerCode,
            FilterBillerCode: selectedBillerCode
        }),
    })
        .then((res) => res.json())
        .then((data) => {
            setBillingInfo(data);
            setLoading(false)
        });
    }, [startDate, selectedBillerCode, user.BillerCode]);
  
    useEffect(() => {
      fetchPaymentDB();
    }, [startDate, selectedBillerCode, fetchPaymentDB]);
    

    // Start: Selecting Rows //
    const [selectedRows, setSelectedRows] = useState([]);
    const [selectAll, setSelectAll] = useState(false);
    const handleRowSelected = (state) => {
        setSelectedRows(state.selectedRows);
        setSelectAll(state.selectedCount === data.length);
    };
    // End: Selecting Rows //

    // Start: Exporting Files  //
    const handleExportXLSX = () => {
        const rowsToExport = selectAll ? data : selectedRows;
        downloadXLSX(rowsToExport);
    };
    const handleExportCSV = () => {
        const rowsToExport = selectAll ? data : selectedRows;
        downloadCSV(rowsToExport);
    };
    const handleExportJSON = () => {
        const rowsToExport = selectAll ? data : selectedRows;
        downloadJSON(rowsToExport);
    };
    //End: Exporting Files //
 
    //Start: Modifying Columns //
    const data = billingInfo.map((billingItem) => ({
        id: billingItem.Id,
        billNum: billingItem.cBillNum,
        bankRef: billingItem.cBankRef,
        billDate: billingItem.cBillDate,
        dueDate: billingItem.cDuedate,
        billAmount: billingItem.nBillAmnt,
        billPeriod:billingItem.cBillPeriod,
        currentBalance: billingItem.nCurrentBal,
        status: billingItem.nStatus, 
        insertDate: billingItem.cInsertDate, 
        modifiedDate: billingItem.cModifiedDate, 
        disconnectionStatus: billingItem.cDisconnectionDate, 
        accountStatus: billingItem.nAccountStatus    
    }));
    
    const initialColumns  = [
        {
            name: 'View',
            selector: (row) => (
                <Button onClick={() => handleViewButtonClick(row)}>
                    View
                </Button>
            ),
            sortable: true,
            visible: true,
        },        
        {
            name: 'Bill Number',
            selector: row => row.billNum,
            sortable: true,
            visible: true,
        },
        {
            name: 'Bank Ref.',
            selector: row => row.bankRef,
            sortable: true,
            visible: true,
            conditionalCellStyles: [
                {
                    when: (row) => row.payment < 300,
                    style: {
                      backgroundColor: 'pink',
                      color: 'black',
                      '&:hover': {
                          cursor: 'pointer',
                        },
                    },
                  },
            ]
        },
        {
            name: 'Bill Date',
            selector: row => row.billDate,
            sortable: true,
            visible: true,
        },
        {
            name: 'Due Date',
            selector: row => row.dueDate,
            sortable: true,
            visible: true,
        },
        {
            name: 'Bill Amount',
            selector: row => row.billAmount,
            sortable: true,
            visible: true,
        },
        {
            name: 'Bill Period',
            selector: row => row.billPeriod,
            sortable: true,
            visible: true,
        },
        {
            name: 'Current Balance',
            selector: row => row.currentBalance,
            sortable: true,
            visible: true,
        },
        {
            name: 'Status',
            selector: row => row.status,
            sortable: true,
            visible: true,
        },
        {
            name: 'Ins. Date',
            selector: row => row.insertDate,
            sortable: true,
            visible: true,
        },
       
        {
            name: 'Mod. Date',
            selector: row => row.modifiedDate,
            sortable: true,
            visible: true,
            conditionalCellStyles: [
                {
                    when: (row) => {
                        const currentDate = new Date();
                        const modifiedDate = new Date(row.modifiedDate);
        
                        return (
                            currentDate.getDate() !== modifiedDate.getDate() ||
                            currentDate.getMonth() !== modifiedDate.getMonth() ||
                            currentDate.getFullYear() !== modifiedDate.getFullYear()
                        );
                    },
                    style: {
                        backgroundColor: 'pink',
                        color: 'black',
                        '&:hover': {
                            cursor: 'pointer',
                        },
                    },
                },
            ],
        },     
        {
            name: 'Disc. Date',
            selector: row => row.disconnectionStatus,
            sortable: true,
            visible: true,
        },
        {
            name: 'Account Status',
            selector: row => row.accountStatus,
            sortable: true,
            visible: true,
        }
    ];
    //End: Modifying Columns //

    //Start: Filter Columns //
    const [selectedColumns, setSelectedColumns] = useState(initialColumns);
    const visibleColumns = selectedColumns.filter((column) => column.visible);
    
    const toggleColumnVisibility = (columnName) => {
        const updatedColumns = selectedColumns.map((column) => {
            if (column.name === columnName) {
                return { ...column, visible: !column.visible };
            }
            return column;
        });
        setSelectedColumns(updatedColumns);
    };
    
    const resetColumnVisibility = () => {
        const resetColumns = initialColumns.map((column) => ({
            ...column,
            visible: true,
        }));
        setSelectedColumns(resetColumns);
    };
    
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    
    const handleDropdownToggle = () => {
        setIsDropdownOpen(!isDropdownOpen);
    };
    
    const handleOkClick = () => {
        setIsDropdownOpen(false);
    };
    //End: Filter Columns //
    
    //Start: Filter Billers //
    const handleBillerCodeSelect = (e) => {
        const selectedCode = parseInt(e.target.value, 10);
        setSelectedBillerCode(selectedCode);
      };

    function BillerCodeSection({ user, selectedBillerCode, handleBillerCodeSelect, billerCodeOptions }) {
        if (user.Role === 1 || user.Role === 2) {
            return (
                <GridItem colSpan={3}>
                    <Select
                        value={selectedBillerCode}
                        onChange={handleBillerCodeSelect}
                        border="1px"
                        borderRadius='10px'
                        borderColor="gray.400"
                    >
                        <option value="" disabled={selectedBillerCode !== ''}>Branches List</option>
                        {billerCodeOptions.map((biller) => (
                            <option key={biller.value} value={biller.value}>
                                {biller.label}
                            </option>
                        ))}
                    </Select>
                </GridItem>
            );
        } else {
            return null; 
        }
    }
    //End: Filter Billers //
    
    const [searchQuery, setSearchQuery] = useState("");
    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };

    const filteredData = data.filter((row) => {  
    const isOtherCriteriaMatch =
    
    (row.id && row.id.toString().includes(searchQuery)) ||  
    (row.billNum && row.billNum.includes(searchQuery)) || 
    (row.bankRef && row.bankRef.includes(searchQuery)) ||
    (row.billDate && row.billDate.includes(searchQuery)) ||
    (row.dueDate && row.dueDate.includes(searchQuery)) ||
    (row.billAmount && row.billAmount.toString().includes(searchQuery)) ||
    (row.billPeriod && row.billPeriod.includes(searchQuery)) ||
    (row.currentBalance && row.currentBalance.toString().includes(searchQuery)) ||
    (row.status && row.status.toString().includes(searchQuery)) ||
    (row.insertDate && row.insertDate.includes(searchQuery)) ||
    (row.modifiedDate && row.modifiedDate.includes(searchQuery)) ||
    (row.disconnectionStatus && row.disconnectionStatus.includes(searchQuery)) ||
    (row.accountStatus && row.accountStatus.toString().includes(searchQuery));
    return isOtherCriteriaMatch;
    });

    //Start: Data Table Modification // 
    const paginationOptions = {
        rowsPerPageText: "Rows per page:",
        rangeSeparatorText: "of",
        selectAllRowsItem: true,
        selectAllRowsItemText: "All",
    };
    const DataTableCustomStyles = {
    headRow: {
        style: {
            border: 'none',
        },
    },
    headCells: {
        style: {
            color: '#202124',
            fontSize: '14px',
        },
    },
    pagination: {
        style: {
            border: 'none',
        },
    }
    };
    //End: Data Table Modification // 

    //Start: Date Picker Modification // 
    const renderMonthContent = (month, shortMonth, longMonth) => {
        const tooltipText = `Tooltip for month: ${longMonth}`;
        return <span title={tooltipText}>{shortMonth}</span>;
    };
    const onChange = (newDate) => {
    if (
        newDate.getMonth() !== previousDate.getMonth() ||
        newDate.getFullYear() !== previousDate.getFullYear()
    ) 
    // Update the previous date
    setPreviousDate(newDate);
    // Update the selected date
    setStartDate(newDate);
    
    };  
    //End: Date Picker Modification // 

    // Start: View Modal //
    const [isModalOpen, setIsModalOpen] = useState(false);
    const openModal = () => {
        setIsModalOpen(true);
    };
    const closeModal = () => {
        setIsModalOpen(false);
    };
    const handleViewButtonClick = (row) => {
        CustInfo(row.bankRef);
        CustomerBillingTable(row.bankRef);
        CustomerPaymentTable(row.bankRef);
        openModal();
    };
    // End: View Modal //

    // Start: Modal Customer Information //
    const [CustomerInfo, setCustomerInfo] = useState('');
    function CustInfo(bankRef) {
        fetch(`${process.env.REACT_APP_API_URL}/pavibills/customerinfo`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                AtmRef: bankRef
            }),
        })
        .then((res) => res.json())
        .then((data) => {
            setCustomerInfo(data)
        })
    };
    function getAccountStatus(CustomerInfo) {
        switch (true) {
            case CustomerInfo.AccountStatus === 2:
                return <Badge colorScheme='orange' fontSize='12px' mt="-10px">Inactive</Badge>;
            case CustomerInfo.AccountStatus === 3:
                return <Badge colorScheme='red' fontSize='12px'>Disconnected</Badge>;
            default:
                return <Badge colorScheme='green' fontSize='12px'>Active</Badge>;
        }
    };
    // End: Modal Customer Information //


    // Start: Billing History Modal Table //
    const [BillingHistory, setBillingHistory] = useState([]);
    const [loading2, setLoading2] = useState(false);
    
    function CustomerBillingTable(bankRef) {
        setLoading2(true);
        fetch(`${process.env.REACT_APP_API_URL}/customerportal/customerbillinghistory`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                AtmRef: bankRef
            }),
        })
        .then((res) => res.json())
        .then((data) => {
            console.log('Bank/ATM Ref:', bankRef)
            setBillingHistory(data);
            setLoading2(false);
        })
    };

    const BillingHistoryDataInit = BillingHistory.map((BillingtHistoryItem) => ({
        BillNumber: BillingtHistoryItem.BillNumber,
        BillingMonth: BillingtHistoryItem.BillingMonth,
        BillingPeriod: BillingtHistoryItem.BillingPeriod,
        TotalCharge: BillingtHistoryItem.TotalCharge,
        Usage: BillingtHistoryItem.Usage,
        DueDate: BillingtHistoryItem.DueDate,
        DisconDate: BillingtHistoryItem.DisconDate,
        IsPaid: BillingtHistoryItem.IsPaid,
        PaymentMode: BillingtHistoryItem.PaymentMode,
        PaymentDate: BillingtHistoryItem.PaymentDate
    }));
    const BillingMinRows = 10;
    const BillingEmptyRow = {
        BillNumber: '',
        BillingMonth: '',
        BillingPeriod: '',
        TotalCharge: '',
        Usage: '',
        DueDate: '',
        DisconDate: '',
        IsPaid: '',
        PaymentMode: '',
        PaymentDate: ''
    };
    const BillingEmptyRows = Array.from({ length: Math.max(0, BillingMinRows - BillingHistoryDataInit.length) }, () => BillingEmptyRow);
    const BillingHistoryData = BillingHistoryDataInit.length >= 1 ? BillingHistoryDataInit.concat(BillingEmptyRows) : BillingHistoryDataInit;
    const BillingHistoryColumns = [
        {
            name: 'Bill Number',
            selector: (row) => row.BillNumber,
            sortable: true,
            visible: true,
        },
        {
            name: 'Billing Month',
            selector: (row) => row.BillingMonth,
            sortable: true,
            visible: true,
        },
        {
            name: 'Bill Period',
            selector: (row) => row.BillingPeriod,
            sortable: true,
            visible: true,
        },
        {
            name: 'Total Charge',
            selector: (row) => row.TotalCharge,
            sortable: true,
            visible: true,
        },
        {
            name: 'Consumption',
            selector: (row) => row.Usage,
            sortable: true,
            visible: true,
        },
        {
            name: 'Due Date',
            selector: (row) => row.DueDate,
            sortable: true,
            visible: true,
        },
        {
            name: 'Disc. Date',
            selector: (row) => row.DisconDate,
            visible: true,
        },
        {
            name: 'Payment Status',
            selector: (row) => row.IsPaid,
            sortable: true,
            visible: true,
        },
        {
            name: 'Payment Source',
            selector: (row) => row.PaymentMode,
            sortable: true,
            visible: true,
        },
        {
            name: 'Payment Date',
            selector: (row) => row.PaymentDate,
            sortable: true,
            visible: true,
        },
    ];

    const BillingExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    // End: Billing History Modal Table //

    // Start: Payment History Modal Table //
    const [PaymentHistory, setPaymentHistory] = useState([]);
    const [loading3, setLoading3 ] = useState(false);
    
    function CustomerPaymentTable(bankRef) {
        setLoading3(true);
        fetch(`${process.env.REACT_APP_API_URL}/billspayment/paymenthistory`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                AtmRef: bankRef
            }),
        })
        .then((res) => res.json())
        .then((data) => {
            console.log(bankRef);
            setPaymentHistory(data);
            setLoading3(false);
        })
    };
    const PaymentHistoryDataInit = PaymentHistory.map((PaymentHistoryItem) => ({
        PaymentRef: PaymentHistoryItem.PaymentRef,
        Payment: PaymentHistoryItem.Payment,
        PaymentDate: PaymentHistoryItem.PaymentDate,
        DueDate: PaymentHistoryItem.Duedate,
        PaymentCenter: PaymentHistoryItem.PaymentCenterName,
        TxnRef: PaymentHistoryItem.TxnRef,
        PayMongoSource: PaymentHistoryItem.PayMonggoSource
    }));    
    const PaymentMinRows = 10;
    const PaymentEmptyRow = {
      PaymentRef: '',
      Payment: '',
      PaymentDate: '',
      DueDate: '',
      PaymentCenter: '',
      TxnRef: '',
      PaymentStatus: '',
      PaymentMode: '',
      PayMonggoSource: '',
    };
    const PaymentEmptyRows = Array.from({ length: Math.max(0, PaymentMinRows - PaymentHistoryDataInit.length) }, () => PaymentEmptyRow);
    const PaymentHistoryData = PaymentHistoryDataInit.length >= 1 ? PaymentHistoryDataInit.concat(PaymentEmptyRows) : PaymentHistoryDataInit;
    const PaymentHistoryColumns = [
        {
            name: 'Payment Reference',
            selector: (row) => row.PaymentRef,
            sortable: true,
            visible: true,
        },
        {
            name: 'Payment',
            selector: (row) => row.Payment,
            sortable: true,
            visible: true,
        },
        {
            name: 'Payment Date',
            selector: (row) => row.PaymentDate,
            sortable: true,
            visible: true,
        },
        {
            name: 'Due Date',
            selector: (row) => row.DueDate,
            sortable: true,
            visible: true,
        },
        {
            name: 'Payment Center',
            selector: (row) => row.PaymentCenter,
            sortable: true,
            visible: true,
        },
        {
            name: 'Transaction Nunber',
            selector: (row) => row.TxnRef,
            visible: true,
        },
        {
            name: 'Status',
            selector: (row) => row.PaymentStatus,
            sortable: true,
            visible: true,
        },
        {
            name: 'Payment Source',
            selector: (row) => row.PaymentMode,
            sortable: true,
            visible: true,
        },
        {
            name: 'PayMongo Source',
            selector: (row) => row.PayMongoSource,
            sortable: true,
            visible: true,
        },
    ];
    const PaymentExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;

    // End: Payment History Modal Table //

    return (
        <>
        <Box>                
            <Text fontSize="2rem" fontWeight="bold">Billings</Text>
            <Grid templateColumns='repeat(12, 1fr)' templateRows='repeat(2, 1fr)' gap={2}  mt='1rem'>
                <GridItem colSpan={3}>
                    <InputGroup>
                        <InputLeftElement pointerEvents="none" children={<SearchIcon color="gray.700" />}/>
                        <Input
                        type="text"
                        placeholder="Search data from table..."
                        value={searchQuery}
                        onChange={handleSearchChange} 
                        border="1px"
                        borderRadius='10px'
                        borderColor="gray.400"
                        outline="none"
                        />
                    </InputGroup>
                    </GridItem>

                <GridItem colSpan={1} colStart={10}>
                    <Tooltip label="Must select a row" isDisabled={selectedRows.length > 0}>
                        <Button onClick={handleExportXLSX} cursor="pointer" isDisabled={selectedRows.length === 0}  border="1px"
                        borderColor="gray.400" borderRadius='10px'>
                            Export XLSX
                        </Button>
                    </Tooltip>
                </GridItem>


                <GridItem colSpan={1}>
                    <Tooltip label="Must select a row" isDisabled={selectedRows.length > 0}>
                        <Button onClick={handleExportCSV} cursor="pointer" isDisabled={selectedRows.length === 0}  border="1px"
                        borderColor="gray.400" borderRadius='10px'>
                            Export CSV
                        </Button>
                    </Tooltip>
                </GridItem>

                <GridItem colSpan={1}>
                    <Tooltip label="Must select a row" isDisabled={selectedRows.length > 0}>
                        <Button onClick={handleExportJSON}  cursor="pointer" isDisabled={selectedRows.length === 0}  border="1px"
                        borderColor="gray.400" borderRadius='10px'>
                            Export JSON
                        </Button>
                    </Tooltip>
                </GridItem>

                <GridItem colSpan={3}>
                    <BillerCodeSection
                    user={user}
                    selectedBillerCode={selectedBillerCode}
                    handleBillerCodeSelect={handleBillerCodeSelect}
                    billerCodeOptions={billerCodeOptions}
                    />
                </GridItem>
                
                <GridItem colSpan={2} colStart={10}>
                    <Box h='100%' border='1px' borderColor='gray.400' borderRadius='10px' alignItems='center' display='flex'>
                        <DatePicker
                        selected={startDate}
                        onChange={onChange}
                        renderMonthContent={renderMonthContent}
                        showMonthYearPicker
                        dateFormat='MM/yyyy'
                        />
                    </Box>
                </GridItem>
                   
                <GridItem colSpan={1} >    
                    <Menu isOpen={isDropdownOpen} >
                        <Tooltip label='Filter Columns'>
                            <MenuButton 
                            onClick={handleDropdownToggle} 
                            backgroundColor='white' 
                            borderRadius='10px'
                            border="1px" 
                            borderColor="gray.400"
                            height='100%'
                            width='40%'>                          
                                <Grid templateColumns='repeat(3, 1fr)' >
                                    <GridItem colSpan={1} colStart={2}>
                                        <FcFilledFilter/>
                                    </GridItem>
                                </Grid>
                            </MenuButton>
                        </Tooltip>
                        <MenuList>
                        {selectedColumns.map((column) => (
                            <MenuItem key={column.name}>
                                <Checkbox
                                isChecked={column.visible}
                                onChange={() => toggleColumnVisibility(column.name)}
                                >
                                {column.name}
                                </Checkbox>
                            </MenuItem>
                        ))}
                            <MenuItem>
                                <Grid templateColumns='repeat(5, 1fr)' >
                                    <GridItem colSpan={1} colStart={2}>
                                        <Text onClick={handleOkClick} className="buttonCol">
                                            Done
                                        </Text>
                                    </GridItem>
                                    <GridItem colSpan={1} colStart={4}>
                                        <Text onClick={resetColumnVisibility} className="buttonCol">
                                            Reset
                                        </Text>
                                    </GridItem>
                                </Grid>                               
                            </MenuItem>
                        </MenuList>
                    </Menu>
                </GridItem>
            </Grid>

            <DataTable
                columns={visibleColumns}
                data={filteredData}
                selectableRows
                onSelectedRowsChange={handleRowSelected}
                selectableRowProps={{ checked: selectAll }}
                pagination={true}
                paginationPerPage={10}
                paginationRowsPerPageOptions={[10, 20, 30]}
                paginationTotalRows={filteredData.length}
                paginationServer={false}
                paginationOptions={paginationOptions}
                pointerOnHover={true}
                highlightOnHover={true}
                customStyles={DataTableCustomStyles}
                progressPending={loading}
                progressComponent={<Loader/>}  
                noDataComponent="Please Select a Branch and date"                  
            />
        </Box>

        <Modal blockScrollOnMount={false} isOpen={isModalOpen} onClose={closeModal} size='full'>
        <ModalOverlay />
        <ModalContent 
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.3 }}
        width="80%"
        overflow="auto"
        ml={{ base: 0, lg: '15rem' }}
        my="2rem"
        p="10px"
        >
        <ModalHeader>View Details</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
            
        <Text>{getAccountStatus(CustomerInfo)}</Text>
        <Text><b>Customer's Name:</b> {CustomerInfo.length !== 0 ? CustomerInfo[0].CustomerName : 'Loading customer data...'}</Text>
        <Text><b>ATM Ref:</b> {CustomerInfo.length !== 0 ? CustomerInfo[0].Atmref : 'Loading customer data...'}</Text>
        <Text><b>Account Number:</b> {CustomerInfo.length !== 0 ? CustomerInfo[0].AccountNumber : 'Loading customer data...'}</Text>
        <Text><b>Address:</b> {CustomerInfo.length !== 0 ? CustomerInfo[0].Street + ' ' + CustomerInfo[0].City : 'Loading customer data...'}</Text>

        <Grid templateColumns='repeat(2, 1fr)' templateRows='repeat(1, 1fr)' gap={2} mt='1rem'>
            <GridItem colSpan={1}>
                <Box 
                backgroundColor="white" 
                border="1px" 
                borderColor="gray.400"
                borderRadius="5px"
                p='10px'
                minW='300px'
                maxW='700px'  
                minH='650px'>           
                    <Text>Billing History</Text>
                    <DataTable
                    columns={BillingHistoryColumns}
                    data={BillingHistoryData}
                    noHeader
                    expandableRows
                    expandableRowsHideExpander
                    expandOnRowClicked
                    expandableRowDisabled={(row) => row.BillNumber === ""} 
                    expandableRowsComponent={BillingExpandedComponent}
                    pagination={true}
                    paginationPerPage={10}
                    paginationRowsPerPageOptions={[10, 20, 30]}
                    paginationTotalRows={BillingHistoryData.length}
                    progressPending={loading2}
                    progressComponent={<Loader/>}
                    noDataComponent={<NoResultsFound/>}/>    
                </Box>
            </GridItem>
            
            <GridItem colSpan={1}>
            <Box 
            backgroundColor="white" 
            border="1px" 
            borderColor="gray.400"
            borderRadius="5px"
            p='10px'
            minW='500px'            
            maxW='700px'             
            minH='650px'           
            >
            <Text>Payment History</Text>
            <DataTable
            columns={PaymentHistoryColumns}
            data={PaymentHistoryData}
            noHeader
            expandableRows
            expandableRowsHideExpander
            expandOnRowClicked
            expandableRowDisabled={(row) => row.PaymentRef === ""} 
            expandableRowsComponent={PaymentExpandedComponent}     
            pagination={true}
            paginationPerPage={10}
            paginationRowsPerPageOptions={[10, 20, 30]}
            paginationTotalRows={PaymentHistoryData.length}
            progressPending={loading3}
            progressComponent={<Loader/>}
            noDataComponent={<NoResultsFound/>}       
            />
            </Box>
            </GridItem>
        </Grid>
        </ModalBody>
        </ModalContent>  
        </Modal>
        </>
    )
}
