import { ChakraProvider, Box, Center, Heading, Text, CSSReset, extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
    fonts: {
        heading: "Montserrat, sans-serif",
        body: "Open Sans, sans-serif",
    },
});

export default function ComingSoonPage() {
    return (
        <ChakraProvider theme={theme}>
            <CSSReset />
            <Box height="80vh" bgColor="teal.500" color="white" textAlign="center" padding="4">
                <Center height="100%">
                    <Box>
                        <Heading fontSize="4xl">Coming Soon</Heading>
                        <Text fontSize="xl" marginTop="4">
                            We're working on something awesome. Stay tuned!
                        </Text>
                    </Box>
                </Center>
            </Box>
        </ChakraProvider>
    );
}
