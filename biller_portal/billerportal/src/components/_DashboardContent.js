import { Box, Text, Image, Grid, GridItem, HStack, Flex, VStack } from "@chakra-ui/react";

import { useState, useEffect } from "react";
import totalpaymentsign from "../images/totalpaymentsign.png";
import totalpaymenttodaysign from "../images/totalpaymenttodaysign.png";
import synchedsign from "../images/synchedsign.png";
import tableicon from "../images/tableicon.png";
import unsynchedsign from "../images/unsychedsign.png";
import { Loader, Loader1 } from "./loader";
import { useContext } from "react";
import UserContext from "../UserContext";
import DataTable from "react-data-table-component";
import { billerCodeOptions } from "./billerList";

import { connect } from "react-redux";
import { fetchBillerListData, setBillerListData, fetchTotalPaymentRequest, setTotalPayment, fetchTotalPaymentTodayRequest, setTotalPaymentToday, fetchSynchedBillsRequest, setSynchedBills, fetchOutOfSynchedBillsRequest, setOutOfSynchedBills } from "../_actions/dashboardActions";

function DashboardStatistics({ billerListData, fetchBillerListData, totalPayment, setTotalPayment, totalPaymentLoading, totalPaymentToday, setTotalPaymentToday, totalPaymentTodayLoading, synchedBills, setSynchedBills, synchedBillsLoading, outOfSynchedBills, setOutOfSynchedBills, outOfSynchedBillsLoading }) {
    const { user } = useContext(UserContext);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await fetchBillerListData();
                setBillerListData(data);

                fetchTotalPaymentRequest();
                const totalPaymentData = await fetch(`${process.env.REACT_APP_API_URL}/billspayment/totalpayment`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        BillerCode: user.BillerCode,
                    }),
                }).then((res) => res.json());
                setTotalPayment(totalPaymentData);
                console.log("Total Payment:", totalPayment);



                fetchTotalPaymentTodayRequest();
                const totalPaymentTodayData = await fetch(`${process.env.REACT_APP_API_URL}/billspayment/totalpaymenttoday`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        BillerCode: user.BillerCode,
                    }),
                }).then((res) => res.json());
                setTotalPaymentToday(totalPaymentTodayData);
                console.log("Total Payment Today:", totalPaymentToday);


                fetchSynchedBillsRequest();
                const synchedBillsData = await fetch(`${process.env.REACT_APP_API_URL}/pavibills/synchedbills`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        BillerCode: user.BillerCode,
                    }),
                }).then((res) => res.json());
                setSynchedBills(synchedBillsData);
                console.log("Synched Bills:", synchedBills);

                fetchOutOfSynchedBillsRequest();
                const outOfSynchedBillsData = await fetch(`${process.env.REACT_APP_API_URL}/pavibills/outofsyncbills`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        BillerCode: user.BillerCode,
                    }),
                }).then((res) => res.json());
                setOutOfSynchedBills(outOfSynchedBillsData);
                console.log("Out of Synched Bills:", outOfSynchedBills);

            } catch (error) {
                console.error("Error fetching data:", error);
            }
        };

        fetchData();
    }, [user.BillerCode, fetchBillerListData, setTotalPayment]);

    const columns = [
        {
            name: "Biller Name",
            selector: (row) => row.billerName,
        },
        {
            name: "Biller Code",
            selector: (row) => row.billerCode,
        },
        {
            name: "Total Number of Accounts",
            selector: (row) => row.totalAccounts,
        },
    ];

    const data = billerListData.map((billerListItem) => ({
        billerCode: billerListItem.BillerCode,
        billerName: billerCodeOptions.find((option) => option.value === billerListItem.BillerCode)?.label,
        totalAccounts: billerListItem.TotalAccounts,
    }));

    const formatNumberWithCommas = (number) => {
        if (typeof number !== "number" && number !== 0) {
            return "";
        }
        return number.toLocaleString();
    };

    const cardsPaymentData = [
        {
            title: "Total Count",
            description: formatNumberWithCommas(totalPayment),
            image: totalpaymentsign,
        },
    ];

    const cardsPaymentTodayData = [
        {
            title: "Total Count",
            description: formatNumberWithCommas(totalPaymentToday),
            image: totalpaymenttodaysign,
        },
    ];

    const cardSynchedAccountsData = [
        {
            title: "Total Count",
            description: formatNumberWithCommas(synchedBills),
            image: synchedsign,
        },
    ];

    const cardOutofSyncBillssData = [
        {
            title: "Total Count",
            description: formatNumberWithCommas(outOfSynchedBills),
            image: unsynchedsign,
        },
    ];

    return (
        <>
            <Grid templateColumns={{ base: "repeat(8, 1fr)", xl: "repeat(6, 1fr)" }} templateRows="repeat(5, 1fr)" gap={4}>
                {user.Role !== 3 && user.Role !== 4 && (
                    <GridItem rowSpan={5} colSpan={{ base: "8", xl: "4" }}>
                        <Box w="100%" h="750px" overflowY="auto" border="1px solid #ccc" borderRadius="5px" className="custom-scroll billerListTable" px="20px" py="10px">
                            <HStack>
                                <Image src={tableicon} style={{ width: "50px" }} />
                                <Text fontSize="30px">Biller List</Text>
                            </HStack>

                            <DataTable columns={columns} data={data} fixedHeader pointerOnHover={true} highlightOnHover={true} noDataComponent={<Loader />} className="custom-scroll" />
                        </Box>
                    </GridItem>
                )}

                <GridItem colSpan={{ base: "4", md: "4", xl: "1" }}>
                    {cardsPaymentData.map((card, index) => (
                        <Box key={index} borderWidth="1px" borderRadius="5px" className="statisticCard">
                            <Box className="title" width="100%" height="145px" px="5">
                                {totalPaymentLoading ? (
                                    <Flex justifyContent="center">
                                        <Loader1 />
                                    </Flex>
                                ) : (
                                    <>
                                        <Flex justifyContent="flex-start" pt="20px">
                                            <VStack alignItems="flex-start">
                                                <Image src={card.image} alt={card.title} ml="1rem" className="cardicons" />
                                                <Text fontSize={{ base: "14px", md: "14px" }}>Total Payment Received</Text>
                                                <Text fontSize="18px" color="rgba(241, 113, 28, 1)" className="data">
                                                    {card.description}
                                                </Text>
                                            </VStack>
                                        </Flex>
                                    </>
                                )}
                            </Box>
                            <Box backgroundColor="rgba(241, 113, 28, 1)" width="100%" height="30px" px="5" className="cardLowerContent" display="flex" alignItems="center">
                                <Text fontWeight="bold" fontSize="12px" mb="2" color="#fafdfa">
                                    {card.title}
                                </Text>
                            </Box>
                        </Box>
                    ))}
                </GridItem>

                <GridItem colSpan={{ base: "4", md: "4", xl: "1" }}>
                    {cardsPaymentTodayData.map((card, index) => (
                        <Box key={index} borderWidth="1px" borderRadius="5px" className="statisticCard">
                            <Box className="title" width="100%" height="145px" px="5">
                                {totalPaymentTodayLoading ? (
                                    <Flex justifyContent="center">
                                        <Loader1 />
                                    </Flex>
                                ) : (
                                    <>
                                        <Flex justifyContent="flex-start" pt="20px">
                                            <VStack alignItems="flex-start">
                                                <Image src={card.image} alt={card.title} ml="1rem" className="cardicons" />
                                                <Text fontSize={{ base: "14px", md: "14px" }}>Payment Received Today</Text>
                                                <Text fontSize="18px" color="#56ca00" className="data">
                                                    {card.description}
                                                </Text>
                                            </VStack>
                                        </Flex>
                                    </>
                                )}
                            </Box>
                            <Box backgroundColor="#56ca00" width="100%" height="30px" px="5" className="cardLowerContent" display="flex" alignItems="center">
                                <Text fontWeight="bold" fontSize="12px" mb="2" color="#fafdfa">
                                    {card.title}
                                </Text>
                            </Box>
                        </Box>
                    ))}
                </GridItem>

                <GridItem colSpan={{ base: "4", md: "4", xl: "1" }}>
                    {cardSynchedAccountsData.map((card, index) => (
                        <Box key={index} borderWidth="1px" borderRadius="5px" className="statisticCard">
                            <Box className="title" width="100%" height="145px" px="5">
                                {synchedBillsLoading ? (
                                    <Flex justifyContent="center">
                                        <Loader1 />
                                    </Flex>
                                ) : (
                                    <>
                                        <Flex justifyContent="flex-start" pt="20px">
                                            <VStack alignItems="flex-start">
                                                <Image src={card.image} alt={card.title} ml="1rem" className="cardicons" />
                                                <Text fontSize={{ base: "14px", md: "14px" }}>Synched Bills</Text>
                                                <Text fontSize="18px" color="#00B1D2" className="data">
                                                    {card.description}
                                                </Text>
                                            </VStack>
                                        </Flex>
                                    </>
                                )}
                            </Box>
                            <Box backgroundColor="#00B1D2" width="100%" height="30px" px="5" className="cardLowerContent" display="flex" alignItems="center">
                                <Text fontWeight="bold" fontSize="12px" mb="2" color="#fafdfa">
                                    {card.title}
                                </Text>
                            </Box>
                        </Box>
                    ))}
                </GridItem>

                <GridItem colSpan={{ base: "4", md: "4", xl: "1" }}>
                    {cardOutofSyncBillssData.map((card, index) => (
                        <Box key={index} borderWidth="1px" borderRadius="5px" className="statisticCard">
                            <Box className="title" width="100%" height="145px" px="5">
                                {outOfSynchedBillsLoading ? (
                                    <Flex justifyContent="center">
                                        <Loader1 />
                                    </Flex>
                                ) : (
                                    <>
                                        <Flex justifyContent="flex-start" pt="20px">
                                            <VStack alignItems="flex-start">
                                                <Image src={card.image} alt={card.title} ml="1rem" className="cardicons" />
                                                <Text fontSize={{ base: "14px", md: "14px" }}>Out of Synched</Text>
                                                <Text fontSize="18px" color="rgba(192, 57, 43, 1)" className="data">
                                                    {card.description}
                                                </Text>
                                            </VStack>
                                        </Flex>
                                    </>
                                )}
                            </Box>
                            <Box backgroundColor="rgba(192, 57, 43, 1)" width="100%" height="30px" px="5" className="cardLowerContent" display="flex" alignItems="center">
                                <Text fontWeight="bold" fontSize="12px" mb="2" color="#fafdfa">
                                    {card.title}
                                </Text>
                            </Box>
                        </Box>
                    ))}
                </GridItem>
            </Grid>
        </>
    );
}

const mapStateToProps = (state) => ({
    billerListData: state.dashboardBillerList.dashboardBillerListData,
    totalPayment: state.totalPayment.totalPayment,
    totalPaymentLoading: state.totalPayment.loading,
    totalPaymentToday: state.totalPaymentToday.totalPaymentToday,
    totalPaymentTodayLoading: state.totalPaymentToday.loading,
    synchedBills: state.synchedBills.synchedBills,
    synchedBillsLoading: state.synchedBills.loading,
    outOfSynchedBills: state.outOfSynchedBills.outOfSynchedBills,
    outOfSynchedBillsLoading: state.outOfSynchedBills.loading
});

const mapDispatchToProps = {
    fetchBillerListData,
    fetchTotalPaymentRequest,
    setTotalPayment,
    fetchTotalPaymentTodayRequest,
    setTotalPaymentToday,
    fetchSynchedBillsRequest,
    setSynchedBills,
    fetchOutOfSynchedBillsRequest, 
    setOutOfSynchedBills
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardStatistics);
