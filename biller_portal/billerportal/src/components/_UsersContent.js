import React, { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import DataTable from "react-data-table-component";
import { userRoles, userStatus } from './userRole&Status';
import { billerCodeOptions } from "./billerList";
import Swal from 'sweetalert2';


import {
    // Chakra UI components for tables
    Box,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    TableContainer,

    // Chakra UI components for buttons and text
    Button,
    Text,

    // Chakra UI components for layout and modals
    HStack,
    Image,
    Stack,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalFooter,

    // Chakra UI components for forms and input elements
    FormControl,
    FormLabel,
    Input,
    Select,

    // Chakra UI components for grids
    GridItem,
    Grid,
} from "@chakra-ui/react";

import { connect } from "react-redux";
import { fetchUserTableRequest, setUserTable } from "../_actions/tablesActions";

function UsersContent({ userTableData, fetchUserTableRequest, setUserTable, userTableLoading }) {
    const { user } = useContext(UserContext);

    const [isModalOpen, setIsModalOpen] = useState(false);
    const [selectedUser, setSelectedUser] = useState(null);
    const [shouldRefresh, setShouldRefresh] = useState(false);
    const [isOpen, setIsOpen] = useState(false);



    useEffect(() => {
        const fetchUsersList = async () => {
            fetchUserTableRequest();
            try {
                const response = await fetch(`${process.env.REACT_APP_API_URL}/users/allusers`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        BillerCode: user.BillerCode,
                    }),
                });

                if (!response.ok) {
                    throw new Error(`Failed to fetch data: ${response.statusText}`);
                }
                const data = await response.json();
                setUserTable(data);
            } catch (error) {
                console.error("Error fetching user data:", error);
            }
        };

        fetchUsersList();
    }, [user.BillerCode, fetchUserTableRequest, setUserTable, shouldRefresh]);

    const handleViewClick = (row) => {
        setSelectedUser(row);
        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
        setShouldRefresh(true);
    };

    const getStatusColor = (statusValue) => {
        switch (statusValue) {
          case 1: // Pending
            return {fontWeight:'bold', color: 'white', backgroundColor: 'orange', padding: '5px 15px 5px 15px', borderRadius: '25px', minWidth: '80px', display: 'flex', justifyContent: 'center'};
          case 2: // Active
            return {fontWeight:'bold', color: 'white', backgroundColor: 'green', padding: '5px 15px 5px 15px', borderRadius: '25px', minWidth: '145px', display: 'flex', justifyContent: 'center'};
          case 3: // Rejected/Removed
            return {fontWeight:'bold', color: 'white', backgroundColor: 'red', padding: '5px 15px 5px 15px', borderRadius: '25px', minWidth: '80px', display: 'flex', justifyContent: 'center' };
          default:
            return {};
        }
    };

      const paginationOptions = {
        rowsPerPageText: "Rows per page:",
        rangeSeparatorText: "of",
        selectAllRowsItem: true,
        selectAllRowsItemText: "All",
    };
      

    const columns = [
        {
            name: 'Name',
            cell: (row) => (
              <>
                <div>
                  <span style={{ fontSize: '15px', fontWeight: 'bold'}}>{row.FirstName} {row.LastName}</span>
                  <div style={{ fontSize: '10px', color: 'gray' }}>
                    {row.Role && userRoles.find((role) => role.value === row.Role)?.label}
                  </div>
                </div>
              </>
            ),
        },
        {
          name: 'Status',
          cell: (row) => (
            <>
              <div style={getStatusColor(row.Status)}>
                  {row.Status && userStatus.find((status) => status.value === row.Status)?.label}
              </div>
            </>
          ),
        },
        {
            name: 'Email',
            selector: (row) => row.Email,
        },
        {
            name: 'Branch',
            cell: (row) => (
                <>
              <div>
                  {row.BillerCode !== undefined && billerCodeOptions.find((billerCode) => billerCode.value === row.BillerCode)?.label}
              </div>
            </>
            )
        },
        {
          name: '',
          cell: (row) => (
            <Button onClick={() => handleViewClick(row)}>Update</Button>
          ),
        },
      ];

      function UpdateUser(userId) {
        Swal.fire({
          title: 'Are you sure you want to update the users information?',
          text: 'This action cannot be undone!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, update it!',
          cancelButtonText: 'No, cancel',
        }).then((result) => {
          if (result.isConfirmed) {
            fetch(`${process.env.REACT_APP_API_URL}/users/updateuserinfo`, {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  Id: userId,
                  NewStatus: selectedStatus,
                  NewRole: selectedRole,
                  NewPassword: selectedPassword,
                  ApprovedBy: user.Email
                }),
              })
              .then((res) => res.json())
              .then((data) => {
                if (data === true) {
                    Swal.fire('Information Updated!', 'The information has been updated successfully.', 'success');
                    handleClose();
                    setShouldRefresh(false);
                } else {
                    Swal.fire('Cancelled', 'Password update was cancelled.', 'info');
                }
  
              }) 
                
            }
        });
      }
  
        function UpdatePassword(userId) {
          Swal.fire({
            title: 'Are you sure you want to update the password?',
            text: 'This action cannot be undone!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, update it!',
            cancelButtonText: 'No, cancel',
          }).then((result) => {
            if (result.isConfirmed) {
              fetch(`${process.env.REACT_APP_API_URL}/users/updatepassword`, {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  Id: userId,
                  Password: selectedPassword,
                }),
              })
                .then((res) => res.json())
                .then((data) => {
                  if (data === true) {
                      Swal.fire('Password Updated!', 'The password has been updated successfully.', 'success');
                      handleClose();
                      setShouldRefresh(false);
                  } else {
                      Swal.fire('Cancelled', 'Password update was cancelled.', 'info');
                  }
  
                })  
              }
          });
        }
  
      const handleClose = () => {
      setIsOpen(false);
      setShouldRefresh(true);
      };
  
      const [selectedRole, setSelectedRole] = useState('');
      const handleRoleChange = (event) => {
          const newRoleValue = event.target.value;
          setSelectedRole(newRoleValue); 
      };
      const [selectedStatus, setSelectedStatus] = useState(0);
      const handleStatusChange = (event) => {
          const newStatusValue = event.target.value;
          setSelectedStatus(newStatusValue); 
      };
      const [selectedPassword, setSelectedPassword] = useState('');
      const handlePasswordChange = (event) => {
          const newPasswordValue = event.target.value;
          setSelectedPassword(newPasswordValue);
      };
      

    return (
        <>
            <Box>
                <Text fontSize="2rem" fontWeight="bold">
                    Conso Dashboard Users
                </Text>
                <DataTable 
                columns={columns} 
                data={userTableData} 
                fixedHeader 
                pointerOnHover={true} 
                highlightOnHover={true}
                pagination={true}
                paginationPerPage={10}
                paginationRowsPerPageOptions={[10, 20, 30]}
                paginationTotalRows={userTableData.length}
                paginationServer={false}
                paginationOptions={paginationOptions} 
                className="custom-scroll" />
            </Box>

            <Modal isOpen={isModalOpen} onClose={closeModal} size="xl">
                <ModalOverlay />
                <ModalContent>
                    <Box p="5">
                        <Text fontSize="2xl" fontWeight="bold">User Details</Text>
                        {selectedUser && (
                            <>
                                <HStack>
                                <FormControl isReadOnly>
                                <FormLabel  fontSize="13px">First Name</FormLabel>
                                <Input value={selectedUser.FirstName}/>
                                </FormControl>
                                <FormControl isReadOnly>
                                <FormLabel  fontSize="13px">Last Name</FormLabel>
                                <Input value={selectedUser.LastName}/>
                                </FormControl>
                                </HStack>

                                <FormControl isReadOnly>
                                <FormLabel fontSize="13px">Branch</FormLabel>
                                <Input value={selectedUser.BillerCode !== undefined && billerCodeOptions.find((billerCode) => billerCode.value === selectedUser.BillerCode)?.label}/>
                                </FormControl>

                                <FormControl isReadOnly>
                                <FormLabel fontSize="13px">Role</FormLabel>
                                <Select
                                value={selectedRole}
                                onChange={handleRoleChange}
                                >
                                {userRoles.map((option) => (
                                <option key={option.value} value={option.value}>
                                {option.label}
                                </option>
                                ))}
                                </Select>
                                </FormControl>

                                <FormControl isReadOnly>
                                <FormLabel fontSize="13px">Status</FormLabel>
                                <Select
                                value={selectedStatus}
                                onChange={handleStatusChange}
                                >
                                {userStatus.map((option) => (
                                <option key={option.value} value={option.value}>
                                {option.label}
                                </option>
                                ))}
                                </Select>
                                </FormControl>

                                <FormControl isReadOnly>
                                <FormLabel fontSize="13px">Email</FormLabel>
                                <Input value={selectedUser.Email} />
                                </FormControl>

                                <FormControl>
                                <FormLabel fontSize="13px">Password</FormLabel>
                                <Input value={selectedPassword} onChange={handlePasswordChange} type="password" />
                                </FormControl>

                                <FormControl isReadOnly>
                                <FormLabel fontSize="13px">Approved By:</FormLabel>
                                <Input value={selectedUser.ApprovedBy} />
                                </FormControl>

                            </>
                        )}
                    </Box>
                    <ModalFooter>
                    <Grid templateColumns={{base: 'repeat(8, 1fr)', xl: 'repeat(4, 1fr)'}} gap={4}>
                    <GridItem colSpan={1} colStart={2}>
                    <Button 
                    onClick={() => {
                    UpdateUser(selectedUser.Id);
                    closeModal();
                    }}
                    >
                    Update Information
                    </Button>
                    </GridItem>

                    <GridItem colSpan={1}>
                      <Button 
                      onClick={() => {
                      UpdatePassword(selectedUser.Id);
                      closeModal();
                      }}
                      >
                      Change Password
                      </Button>
                      </GridItem>

                      <GridItem colSpan={1}>
                       <Button onClick={closeModal}>Cancel</Button>
                      </GridItem>
                    </Grid>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    );
}

const mapStateToProps = (state) => ({
    userTableData: state.userTableStore.userTableData,
    userTableLoading: state.userTableStore.loading
});

const mapDispatchToProps = {
    fetchUserTableRequest,
    setUserTable
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersContent);
