import React, { useEffect, useState } from "react";

export function ShowTime() {
  const [currentTime, setCurrentTime] = useState(new Date().toLocaleTimeString());
  const [timezone] = useState(Intl.DateTimeFormat().resolvedOptions().timeZone);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentTime(new Date().toLocaleTimeString());
    }, 1000); // Update every second

    return () => clearInterval(intervalId); // Clean up the interval on unmount
  }, []);

  return (
    <div>
      {timezone} - {currentTime}
    </div>
  );
}

export function ShowDate() {
  const currentDate = new Date();

  const formattedDate = currentDate.toLocaleDateString('en-US', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });

  return <>{formattedDate}</>;
}
