import "./styles/loader.css";
import { Center, Text, Image, VStack } from "@chakra-ui/react";
import NoResults1 from "../images/NoResults1.png";

export default function NoResultsFound() {
    return (
        <>
            <VStack mt="10rem">
                <Center bg="white">
                    <Image src={NoResults1} style={{ width: "100px" }} />
                </Center>
                <Center bg="white">
                    <Text fontWeight="400" fontSize="lg">
                        No Available Data
                    </Text>
                </Center>
            </VStack>
        </>
    );
}
