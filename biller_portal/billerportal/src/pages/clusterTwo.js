import {Box, Text} from '@chakra-ui/react'; 
import ClusterTwoContent from '../components/_ClusterTwoContent';

export default function ClusterThree() {
    return(
        <>
    <Box overflowX="auto" pt='3rem'pl={{ md: "20rem"}} pr={{ md:"5rem"}} px={{base: '2rem' }}>     
        <ClusterTwoContent/>
    </Box>
        </>
    )
}