import { useState, useEffect } from 'react';
import { useNavigate  } from 'react-router-dom';
import {billerCodeOptions} from '../components/billerList';

import Swal from 'sweetalert2';
import pwlogo from '../images/PrimeWaterCircle.png';

import {
  // Chakra UI components for basic UI elements
  Button,
  Stack,
  Card,
  CardBody,
  FormControl,
  Input,
  Image,
  Center,
  Select,
  ChakraProvider,

  // Chakra UI components for forms and labels
  FormLabel,

  // Chakra UI components for extending themes
  extendTheme,
} from '@chakra-ui/react';

const activeLabelStyles = {
    transform: "scale(0.85) translateY(-24px)"
  };
    export const theme = extendTheme({
        components: {
            Form: {
            variants: {
                floating: {
                container: {
                    _focusWithin: {
                    label: {
                        ...activeLabelStyles
                    }
                    },
                    "input:not(:placeholder-shown) + label, .chakra-select__wrapper + label, textarea:not(:placeholder-shown) ~ label": {
                    ...activeLabelStyles
                    },
                    label: {
                    top: 0,
                    left: 0,
                    zIndex: 2,
                    position: "absolute",
                    backgroundColor: "white",
                    pointerEvents: "none",
                    mx: 3,
                    px: 1,
                    my: 2,
                    transformOrigin: "left top"
                    }
                }
                }
            }
            }
        }
    });

export default function Registration() {

    const navigate = useNavigate();

    const [FirstName, setFirstName] = useState ('');
    const [LastName, setLastName] = useState ('');
    const [Email, setEmail] = useState ('');
    const [Password1, setPassword1] = useState("");
    const [Password2, setPassword2] = useState("");
    const [selectedBillerCode, setSelectedBillerCode] = useState('');
    const handleBillerCodeChange = (event) => {
      setSelectedBillerCode(event.target.value);
    };
  
    const [isActive, setIsActive] = useState(false);

    useEffect (() => {
        if((FirstName !== "" && LastName !== "" && Email !== "" && Password1 !== "" && Password2 !== "" && selectedBillerCode !== "" ) && (Password1 === Password2))
        {
            setIsActive (true)
        } else {
            setIsActive (false)
        }
    }, [FirstName, LastName, Email, Password1, Password2, selectedBillerCode]) 

    function registerUser(e) {      
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Email: Email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {

                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Kindly provide another email to complete registration."
                })
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        FirstName: FirstName,
                        LastName: LastName,           
                        Email: Email,
                        Password: Password1,
                        BillerCode: selectedBillerCode,
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true) {

                        // Clear input fields
                        setFirstName("");
                        setLastName("");
                        setEmail("");
                        setPassword1("");
                        setPassword2("");
                        setSelectedBillerCode("");

                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Please wait for your admin to approve your account"
                        })

                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }

                })
            }
        })
    };

    
    
    return(
    <>
    <div className='backgroundone'>
    <ChakraProvider theme={theme}>
        <Center width={"100vw"} height={"100vh"} alignContent={"center"} justifyContent={"center"} position='relative' className='loginCenter'>
            <Card className='loginCard' p='10'>
                <CardBody className='loginCardBody'>
                    <section className='loginTitle'>
                        <Image
                        src={pwlogo}
                        alt='Primewater Logo'
                        className='pwlogo'
                        borderRadius='xl'
                        />
                        <h6>Biller Portal</h6>
                    </section>
                    <section className='formTitle'>
                        <h6>Conso Dashboard</h6>
                        <p>Real-time, updated billing information, and transparency.</p>
                    </section>
                    
                    <form onSubmit={(e) => registerUser(e)}>
                    <Stack spacing={4} align='center' >
                        
                    <FormControl variant="floating" id="first-name">
                    <Input 
                        placeholder=" " 
                        type="text"
                        value={FirstName}
                        onChange={(e) => {
                        const inputName = e.target.value;
                        const parts = inputName.split(' ');
                        const formattedValue = parts
                            .map((part) => {
                            return part.charAt(0).toUpperCase() + part.slice(1).toLowerCase();
                            })
                            .join(' ');
                        setFirstName(formattedValue);
                        }} 
                    />
                    <FormLabel color="gray.500" fontSize="15px" fontWeight="normal">First Name</FormLabel>              
                    </FormControl>

                    <FormControl variant="floating" id="last-name">
                    <Input 
                        placeholder=" " 
                        type="text"
                        value={LastName}
                        onChange={(e) => {
                        const inputLastName = e.target.value;
                        const formattedValue = inputLastName.charAt(0).toUpperCase() + inputLastName.slice(1).toLowerCase();
                        setLastName(formattedValue);
                        }} 
                    />
                    <FormLabel color="gray.500" fontSize="15px" fontWeight="normal">Last Name</FormLabel>              
                    </FormControl>


                        <FormControl variant="floating" id="email">
                            <Input 
                            placeholder=" " 
                            type="email"
                            value={Email}
                            onChange={(e) => {setEmail(e.target.value)}}
                            />
                            <FormLabel color="gray.500" fontSize="15px" fontWeight="normal">Email</FormLabel>              
                        </FormControl>

                        <FormControl variant="floating" id="email">
                            <Input 
                            placeholder=" " 
                            type="password" 
                            value={Password1}
                            onChange={(e) => {setPassword1(e.target.value)}}
                            />
                            <FormLabel color="gray.500" fontSize="15px" fontWeight="normal">Password</FormLabel>              
                        </FormControl>

                        <FormControl variant="floating" id="email">
                            <Input 
                            placeholder=" " 
                            type="password" 
                            value={Password2}
                            onChange={(e) => {setPassword2(e.target.value)}}
                            />
                            <FormLabel  color="gray.500" fontSize="14px" fontWeight="normal">Verify Your Password</FormLabel>              
                        </FormControl>

                        <Select
                        placeholder="Select Biller Code"
                        value={selectedBillerCode}
                        onChange={handleBillerCodeChange}
                        color="gray.500" fontSize="14px"
                        >
                        {billerCodeOptions.map((option) => (
                        <option key={option.value} value={option.value}>
                        {option.label}
                        </option>
                        ))}
                        </Select>


                        { isActive ?
                            <Button className="buttonSignUp" colorScheme='purple' variant='solid' type="submit">SIGN UP</Button>
                            :
                            <Button className="buttonSignUp" colorScheme='purple' variant='solid' type="submit" isDisabled>SIGN UP</Button>
                        }
                        
                        <p>Already have an account? <a 
                        href='/login'
                        style={{ color: 'blue'}}>
                        Sign in Instead
                        </a>
                        </p> 
                        
                    </Stack>
                    </form>                   
                </CardBody>                      
            </Card>           
        </Center>
        </ChakraProvider>
    </div>     
    </>
    )
}