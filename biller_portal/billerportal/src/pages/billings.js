import {Box} from '@chakra-ui/react'; 
import BillingsContent from '../components/_BillingsContent';

export default function Billins() {
    return(
        <>
    <Box overflowX="auto" pt='3rem'pl={{ md: "20rem"}} pr={{ md:"5rem"}} px={{base: '2rem' }}>                   
        <BillingsContent/>
    </Box>
        </>
    )
}