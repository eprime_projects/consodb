import { useContext, useState } from "react";
import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import UserContext from "../UserContext";

import { useSelector, useDispatch } from "react-redux";
import { setDefaultIndex } from "../_actions/sidebarActions";

import { ShowDate } from "../components/showDate&Time";

import PrimewaterLogo from "../images/PrimeWaterCircle1.png";
import PrimewaterLogoMobile from "../images/PrimeWater1.png";

import { FiHome, FiCreditCard, FiFileText, FiMenu, FiBell, FiChevronDown, FiDatabase } from "react-icons/fi";

import {
    // Chakra UI components for common UI elements
    Badge,
    IconButton,
    Avatar,
    Box,
    CloseButton,
    Flex,
    HStack,
    VStack,
    Icon,
    useColorModeValue,
    Text,
    Image,
    Heading,
    Stack,
    Divider,

    // Chakra UI components for drawers and menus
    Drawer,
    DrawerContent,
    useDisclosure,
    Menu,
    MenuButton,
    MenuDivider,
    MenuItem,
    MenuList,

    // Chakra UI components for accordions
    Accordion,
    AccordionItem,
    AccordionButton,
    AccordionPanel,
    AccordionIcon,
} from "@chakra-ui/react";

import { userRoles } from '../components/userRole&Status';

export default function Sidebar() {
    const { user } = useContext(UserContext);
    const location = useLocation();

    const [activeButton, setActiveButton] = useState(getActiveButtonFromPath(location.pathname));

    function getActiveButtonFromPath(pathname) {
        if (pathname === "/dashboard") {
            return "dashboard";
        } else if (pathname === "/billings") {
            return "billings";
        } else if (pathname === "/payments") {
            return "payments";
        } else if (pathname === "/mainconso") {
            return "mainconso";
        } else if (pathname === "/clusterone") {
            return "clusterone";
        } else if (pathname === "/clustertwo") {
            return "clustertwo";
        } else if (pathname === "/users") {
            return "users";
        } else {
            return "dashboard";
        }
    }

    const SidebarContent = ({ onClose, ...rest }) => {
        const defaultIndex = useSelector((state) => state.sidebar);
        const dispatch = useDispatch();

        const handleLinkClick = (index) => {
            dispatch(setDefaultIndex([index]));
        };
        const activeButtonStyle = {
            color: "#f8b739",
        };

        return (
            <Box
                transition="3s ease"
                bg={useColorModeValue("#151c27")}
                color="#dde2e4"
                //borderRight="1px"
                // borderRightColor={useColorModeValue('#f4f5fa')}
                w={{ base: "full", md: "17rem" }}
                pos="fixed"
                h="full"
                {...rest}
                fontSize="8px"
            >
                <Flex h="20" alignItems="center" mx="5" justifyContent="space-between">
                    <Box display="flex" alignItems="center">
                        <Image src={PrimewaterLogo} alt="Primewater Logo" style={{ width: "50px" }} />
                        <Text fontSize="16px" fontWeight="500" textTransform="uppercase" ml={2}>
                            Conso Dashboard
                        </Text>
                    </Box>
                    <CloseButton display={{ base: "flex", md: "none" }} onClick={onClose} />
                </Flex>

                <Stack ml="2rem" mt="3rem" gap={4}>
                    <Link to="/dashboard" className={`sidebarItems ${activeButton === "dashboard" ? "activeLink" : ""}`} onClick={() => setActiveButton("dashboard")} style={{ color: activeButton === "dashboard" ? activeButtonStyle : "" }}>
                        <HStack>
                            <Icon as={FiHome} fontSize="18px" mt="-6px" />
                            <Text>Dashboard</Text>
                        </HStack>
                    </Link>

                    <Link to="/billings" className={`sidebarItems ${activeButton === "billings" ? "activeLink" : ""}`} onClick={() => setActiveButton("billings")} style={{ color: activeButton === "billings" ? activeButtonStyle : "" }}>
                        <HStack>
                            <Icon as={FiFileText} fontSize="18px" mt="-6px" />
                            <Text>Billings</Text>
                        </HStack>
                    </Link>

                    <Link to="/payments" className={`sidebarItems ${activeButton === "payments" ? "activeLink" : ""}`} onClick={() => setActiveButton("payments")} style={{ color: activeButton === "payments" ? activeButtonStyle : "" }}>
                        <HStack>
                            <Icon as={FiCreditCard} fontSize="18px" mt="-6px" />
                            <Text>Payments</Text>
                        </HStack>
                    </Link>

                    <Accordion allowToggle defaultIndex={defaultIndex} onChange={setDefaultIndex}>
                        <AccordionItem borderColor="transparent" ml="-1.1rem">
                            <Text fontSize="16px">
                                <AccordionButton
                                    onClick={(e) => {
                                        handleLinkClick(1); // Update the defaultIndex
                                    }}
                                >
                                    <Box textAlign="left">
                                        <Icon as={FiDatabase} fontSize="20px" mr="0.5rem" />
                                        Cluster Synching
                                    </Box>
                                    <AccordionIcon />
                                </AccordionButton>
                            </Text>
                            <AccordionPanel pb={4}>
                                <Stack ml="1rem" gap={4}>
                                    <Link
                                        to="/mainconso"
                                        className={`sidebarItems ${activeButton === "mainconso" ? "activeLink" : ""}`}
                                        onClick={(e) => {
                                            setActiveButton("mainconso");
                                            handleLinkClick(0);
                                        }}
                                        style={{ color: activeButton === "mainconso" ? activeButtonStyle : "" }}
                                    >
                                        <HStack>
                                            <Icon as={FiHome} fontSize="18px" mt="-6px" />
                                            <Text>Main Conso</Text>
                                        </HStack>
                                    </Link>

                                    <Link
                                        to="/clusterone"
                                        className={`sidebarItems ${activeButton === "clusterone" ? "activeLink" : ""}`}
                                        onClick={(e) => {
                                            setActiveButton("clusterone");
                                            handleLinkClick(0);
                                        }}
                                        style={{ color: activeButton === "clusterone" ? activeButtonStyle : "" }}
                                    >
                                        <HStack>
                                            <Icon as={FiHome} fontSize="18px" mt="-6px" />
                                            <Text>Cluster 1</Text>
                                        </HStack>
                                    </Link>

                                    <Link
                                        to="/clustertwo"
                                        className={`sidebarItems ${activeButton === "clustertwo" ? "activeLink" : ""}`}
                                        onClick={(e) => {
                                            setActiveButton("clustertwo");
                                            handleLinkClick(0);
                                        }}
                                        style={{ color: activeButton === "clustertwo" ? activeButtonStyle : "" }}
                                    >
                                        <HStack>
                                            <Icon as={FiFileText} fontSize="18px" mt="-6px" />
                                            <Text>Cluster 2</Text>
                                        </HStack>
                                    </Link>

                                    <Link
                                        to="/clusterthree"
                                        className={`sidebarItems ${activeButton === "clusterthree" ? "activeLink" : ""}`}
                                        onClick={(e) => {
                                            setActiveButton("clusterthree");
                                            handleLinkClick(0);
                                        }}
                                        style={{ color: activeButton === "clusterthree" ? activeButtonStyle : "" }}
                                    >
                                        <HStack>
                                            <Icon as={FiCreditCard} fontSize="18px" mt="-6px" />
                                            <Text>Cluster 3</Text>
                                        </HStack>
                                    </Link>
                                </Stack>
                            </AccordionPanel>
                        </AccordionItem>
                    </Accordion>
                </Stack>

                {user.Role === 1 || user.Role === 2 ? (
                    <>
                        <Box position="relative" padding="5">
                            <Text ml="1" fontSize="sm">
                                Admin Access
                            </Text>
                            <Divider width="90%" height="auto" />
                        </Box>
                        <Stack ml="2rem" mt="1rem" gap={4} display="flex" alignContent="end">
                            <Link to="/users" className={`sidebarItems ${activeButton === "users" ? "activeLink" : ""}`} onClick={() => setActiveButton("users")} style={{ color: activeButton === "users" ? activeButtonStyle : "" }}>
                                Users
                            </Link>
                        </Stack>
                    </>
                ) : null}

                <VStack display={{ base: "none", md: "flex" }} flexDirection="column" alignItems="center" position="absolute" bottom="0" left="0" right="0">
                    <Heading fontSize="sm" mb="2">
                        © 2024 ePrime Company
                    </Heading>
                </VStack>
            </Box>
        );
    };

    const MobileNav = ({ onOpen, ...rest }) => {
        function getUserBadge(user) {
            switch (true) {
                case user.Role === 1:
                    return (
                        <Badge colorScheme="purple" fontSize="10px" mt="-10px">
                            DevOps
                        </Badge>
                    );
                case user.Role === 2:
                    return (
                        <Badge colorScheme="orange" fontSize="10px">
                            Eprime Admin
                        </Badge>
                    );
                case user.Role === 3:
                    return (
                        <Badge colorScheme="messenger" fontSize="10px">
                            Branch Admin
                        </Badge>
                    );
                default:
                    return (
                        <Badge colorScheme="green" fontSize="10px">
                            User
                        </Badge>
                    );
            }
        }

        return (
            <Flex ml={{ base: 0, md: 60 }} px={{ base: 4, md: 4 }} height="20" alignItems="center" bg={useColorModeValue("#dde2e4")} color="#151c27" justifyContent={{ base: "space-between", md: "flex-end" }} {...rest}>
                <IconButton display={{ base: "flex", md: "none" }} onClick={onOpen} variant="outline" aria-label="open menu" icon={<FiMenu />} />

                <Image display={{ base: "flex", md: "none" }} src={PrimewaterLogoMobile} alt="Primewater Logo" style={{ width: "50px" }} />

                <HStack spacing={{ base: "0", md: "6" }}>
                    <IconButton size="lg" variant="ghost" aria-label="open menu" icon={<FiBell />} />
                    <Flex alignItems={"center"}>
                        <Menu>
                            <MenuButton py={2} transition="all 0.3s" _focus={{ boxShadow: "none" }}>
                                <HStack>
                                    <Avatar size={"sm"} src={"https://images.unsplash.com/photo-1619946794135-5bc917a27793?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=b616b2c5b373a80ffc9636ba24f7a4a9"} />
                                    <VStack display={{ base: "none", md: "flex" }} alignItems="flex-start" spacing="1px" ml="2">
                                        <Text fontSize="lg" as="b">
                                            Hi, {user.FirstName}!
                                        </Text>
                                        <Text>{getUserBadge(user)}</Text>
                                    </VStack>
                                    <Box display={{ base: "none", md: "flex" }}>
                                        <FiChevronDown />
                                    </Box>
                                </HStack>
                            </MenuButton>
                            <MenuList bg={useColorModeValue("white", "gray.900")} borderColor={useColorModeValue("gray.200", "gray.700")}>
                                <MenuItem>Profile</MenuItem>
                                <MenuItem>Settings</MenuItem>
                                <MenuItem>Contact Us</MenuItem>
                                <MenuDivider />
                                <MenuItem as={NavLink} to="/logout">
                                    Sign out
                                </MenuItem>
                            </MenuList>
                        </Menu>
                    </Flex>
                </HStack>
            </Flex>
        );
    };

    const { isOpen, onOpen, onClose } = useDisclosure();
    return (
        <>
            <SidebarContent onClose={onClose} display={{ base: "none", md: "block" }} className="SidebarContent" />
            <Drawer isOpen={isOpen} placement="left" onClose={onClose} returnFocusOnClose={false} onOverlayClick={onClose} size="full">
                <DrawerContent>
                    <SidebarContent onClose={onClose} />
                </DrawerContent>
            </Drawer>
            <MobileNav onOpen={onOpen} />
        </>
    );
}
