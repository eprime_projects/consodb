import {Box} from '@chakra-ui/react'; 
import PaymentsContent from '../components/_PaymentsContent';

export default function Billins() {
    return(
        <>
    <Box overflowX="auto" pt='3rem'pl={{ md: "20rem"}} pr={{ md:"5rem"}} px={{base: '2rem' }}>                   
        <PaymentsContent/>
    </Box>
        </>
    )
};