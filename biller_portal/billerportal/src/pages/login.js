import { Button, Card, CardBody, Stack, Image, Center, FormControl, Input} from '@chakra-ui/react'
import { useState, useEffect, useContext } from 'react'
import { Navigate } from 'react-router-dom'

import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import pwlogo from '../images/PrimeWaterCircle.png'


export default function Login() {

    const {user, setUser} = useContext(UserContext);
    const [Email, setEmail] = useState ('');
    const [Password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(false);

    function authenticate(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Email: Email,
                Password: Password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

            Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Conso Dashboard!"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })
            }
    });
        setEmail('')
        setPassword('')
        
};
    const retrieveUserDetails = (token) => {
            fetch(`${process.env.REACT_APP_API_URL}/users/UserDetails`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                
                setUser({
                Id: data.Id,
                FirstName: data.FirstName,
                LastName: data.LastName,
                Email: data.Email,
                BillerCode: data.BillerCode,
                Role: data.Role,
                Status: data.Status,
                })
            })
        };

    useEffect(() => {
        if((Email !== '' && Password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [Email, Password])


    return (
        (user.Id !== null) ? (
        <Navigate to="/dashboard" />
        ) : (
          <div className='backgroundone'>
            <Center width={"100vw"} height={"100vh"} alignContent={"center"} justifyContent={"center"} position='relative' className='loginCenter'>
                <Card className='loginCard' p='10'>
                    <CardBody className='loginCardBody'>
                        <section className='loginTitle'>
                            <Image
                            src={pwlogo}
                            alt='Primewater Logo'
                            className='pwlogo'
                            borderRadius='xl'
                            />
                            <h6>Biller Portal</h6>
                        </section>
                        <section className='formTitle'>
                            <h6>Conso Dashboard</h6>
                            <p>Please sign in to be updated</p>
                        </section>
                        
                        <form  onSubmit={e => authenticate(e)}>
                        <Stack spacing={4} align='center' >
                            <FormControl>
                                <Input 
                                type="email" 
                                placeholder="Enter email"
                                value={Email}
                                onChange={e => setEmail(e.target.value)}
                                required
                                />
                            </FormControl>

                            <FormControl>
                            <Input 
                            type="password" 
                            placeholder="Password"
                            value={Password}
                            onChange={e => setPassword(e.target.value)}
                            required
                            />
                            </FormControl>
                            { isActive ?
                                <Button className="buttonSignUp" colorScheme='purple' variant='solid' type="submit">SIGN IN</Button>
                                :
                                <Button className="buttonSignUp" colorScheme='purple' variant='solid' type="submit" isDisabled >SIGN IN</Button> 
                            }
                            
                            <p>New on our platform? <a 
                            href='/register'
                            style={{ color: 'blue'}}>
                            Create an account
                            </a>
                            </p>
                        </Stack>
                        </form>                   
                    </CardBody>                      
                </Card>           
            </Center>
          </div>
        )
    );
};