import {Box} from '@chakra-ui/react'; 
import UsersContent from '../components/_UsersContent';

export default function ClusterOne() {
    return(
        <>
    <Box overflowX="auto" pt='3rem'pl={{ md: "20rem"}} pr={{ md:"5rem"}} px={{base: '2rem' }}>                   
        <UsersContent/>
    </Box>
        </>
    )
}