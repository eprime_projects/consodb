import {Box} from '@chakra-ui/react'; 
import ClusterOneContent from '../components/_ClusterOneContent';

export default function ClusterOne() {
    return(
        <>
    <Box overflowX="auto" pt='3rem'pl={{ md: "20rem"}} pr={{ md:"5rem"}} px={{base: '2rem' }}>                   
        <ClusterOneContent/>
    </Box>
        </>
    )
}