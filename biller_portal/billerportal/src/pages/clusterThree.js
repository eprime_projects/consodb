import {Box, Text} from '@chakra-ui/react'; 
import ComingSoonPage from '../components/comingSoon';
import ClusterThreeContent from '../components/_ClusterThreeContent';

export default function ClusterThree() {
    return(
        <>
    <Box overflowX="auto" pt='3rem'pl={{ md: "20rem"}} pr={{ md:"5rem"}} px={{base: '2rem' }}>     
        <ClusterThreeContent/>
    </Box>
        </>
    )
}