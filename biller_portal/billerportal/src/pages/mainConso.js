import {Box, Text} from '@chakra-ui/react'; 
import MainConsoContent from '../components/_MainConsoContent';

export default function MainConso() {
    return(
        <>
    <Box overflowX="auto" pt='3rem'pl={{ md: "20rem"}} pr={{ md:"5rem"}} px={{base: '2rem' }}>   
        <MainConsoContent/>
    </Box>
        </>
    )
}