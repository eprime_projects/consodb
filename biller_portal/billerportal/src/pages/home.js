import UserContext from '../UserContext';
import { useContext } from 'react';
import { Navigate} from 'react-router-dom';


export default function Home(){
    const {user} = useContext(UserContext);

    return(
        <>
        {user.Id == null ? <Navigate to="/login" /> : 
        <Navigate to="/dashboard"/>
        }
        </>
    )
};