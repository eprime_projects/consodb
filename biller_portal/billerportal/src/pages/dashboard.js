import {Box} from '@chakra-ui/react'; 
import DashboardStatistics from "../components/_DashboardContent"

export default function DashboardContent() {
    return(
        <>
        <Box overflowX="auto" mt='3rem'ml={{ md: "20rem"}} mr={{ md:"5rem"}} mx={{base: '2rem' }}>                
            <DashboardStatistics/>
        </Box>
        </>
    )
}