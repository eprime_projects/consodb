export const setClusterOneData = (data) => ({
   type: "SET_CLUSTER_ONE_DATA",
   payload: data,
});

export const fetchClusterOneData = () => (dispatch) => {
   fetch(`${process.env.REACT_APP_API_URL}/pavibills/synchingupdate`, {
       method: "GET",
   })
       .then((res) => res.json())
       .then((data) => {
           dispatch(setClusterOneData(data));
       });
};
