export const FETCH_PAYMENT_TABLE_REQUEST = 'FETCH_PAYMENT_TABLE_REQUEST';
export const fetchPaymentTableRequest = () => ({
    type: FETCH_PAYMENT_TABLE_REQUEST,
});

export const SET_PAYMENT_TABLE = 'SET_PAYMENT_TABLE';
export const setPaymentTable = (paymentTableData) => ({
    type: SET_PAYMENT_TABLE,
    payload: paymentTableData
});

export const SET_START_DATE = 'SET_START_DATE';
export const setStartDate = (startDate) => ({
    type: SET_START_DATE,
    payload: startDate,
});

export const SET_END_DATE = 'SET_END_DATE';
export const setEndDate = (endDate) => ({
    type: SET_END_DATE,
    payload: endDate,
});


export const SET_BILLER_CODE = 'SET_BILLER_CODE'
export const setBillerCode = (billerCode) => ({
    type: SET_BILLER_CODE,
    payload: billerCode
})

export const FETCH_USER_TABLE_REQUEST = 'FETCH_USER_TABLE_REQUEST';
export const fetchUserTableRequest = () => ({
    type: FETCH_USER_TABLE_REQUEST,
});

export const SET_USER_TABLE = 'SET_USER_TABLE';
export const setUserTable = (userTableData) => ({
    type: SET_USER_TABLE,
    payload: userTableData
});


