export const setBillerListData = (data) => ({
    type: "BILLER_LIST_DATA",
    payload: data,
});

export const fetchBillerListData = () => (dispatch) => {
    fetch(`${process.env.REACT_APP_API_URL}/pavibills/billerlist`, {
        method: "GET",
    })
        .then((res) => res.json())
        .then((data) => {
            dispatch(setBillerListData(data));
        });
};

export const FETCH_TOTAL_PAYMENT_REQUEST = "FETCH_TOTAL_PAYMENT_REQUEST";
export const fetchTotalPaymentRequest = () => ({
    type: FETCH_TOTAL_PAYMENT_REQUEST,
});

export const SET_TOTAL_PAYMENT = "SET_TOTAL_PAYMENT";
export const setTotalPayment = (totalPayment) => ({
    type: SET_TOTAL_PAYMENT,
    payload: totalPayment
});

export const FETCH_TOTAL_PAYMENT_TODAY_REQUEST = "FETCH_TOTAL_PAYMENT_TODAY_REQUEST";
export const fetchTotalPaymentTodayRequest = () => ({
    type: FETCH_TOTAL_PAYMENT_TODAY_REQUEST,
});

export const SET_TOTAL_PAYMENT_TODAY = "SET_TOTAL_PAYMENT_TODAY";
export const setTotalPaymentToday = (totalPaymentToday) => ({
    type: SET_TOTAL_PAYMENT_TODAY,
    payload: totalPaymentToday
});

export const FETCH_SYNCHED_BILLS_REQUEST = "FETCH_SYNCHED_BILLS_REQUEST";
export const fetchSynchedBillsRequest = () => ({
    type: FETCH_SYNCHED_BILLS_REQUEST
});

export const SET_SYNCHED_BILLS = "SET_SYNCHED_BILLS";
export const setSynchedBills = (synchedBills) => ({
    type: SET_SYNCHED_BILLS,
    payload: synchedBills
});

export const FETCH_OUT_OF_SYNCHED_BILLS_REQUEST = "FETCH_OUT_OF_SYNCHED_BILLS_REQUEST";
export const fetchOutOfSynchedBillsRequest = () => ({
    type: FETCH_OUT_OF_SYNCHED_BILLS_REQUEST
});

export const SET_OUT_OF_SYNCHED_BILLS = "SET_OUT_OF_SYNCHED_BILLS";
export const setOutOfSynchedBills = (outOfsynchedBills) => ({
    type: SET_OUT_OF_SYNCHED_BILLS,
    payload: outOfsynchedBills
});
