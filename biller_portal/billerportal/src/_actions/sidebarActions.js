import { createSlice } from "@reduxjs/toolkit";

const initialState = [1];

const sidebarSlice = createSlice({
    name: "sidebar",
    initialState,
    reducers: {
        setDefaultIndex: (state, action) => {
            return action.payload;
        },
    },
});

export const { setDefaultIndex } = sidebarSlice.actions;

export default sidebarSlice.reducer;
