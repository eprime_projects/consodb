import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Layout from './Layout';
import { Provider } from 'react-redux'; 
import store from './store';

function App() {
  return (
    <Provider store={store()}>
      <Router>
        <Layout />
      </Router>
    </Provider>
  );
}
export default App;
