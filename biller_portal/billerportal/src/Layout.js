import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { Routes, Route, Navigate, Outlet } from 'react-router-dom';
import Login from './pages/login';
import Registration from './pages/registration';
import Home from './pages/home';
import Logout from './pages/logout';
import PageNotFound from './pages/404';
import DashboardContent from './pages/dashboard';
import BillingsContent from './pages/billings';
import PaymentsContent from './pages/payments';
import MainConso from './pages/mainConso';
import ClusterOne from './pages/clusterOne';
import ClusterTwo from './pages/clusterTwo';
import ClusterThree from './pages/clusterThree';
import UsersContent from './pages/users';
import Sidebar from './pages/sidebar';

import { UserProvider } from './UserContext';

const PrivateRoute = ({ element, authenticated }) => {
  return authenticated ? element : <Navigate to="/login" />;
};

export default function Layout() {
  const location = useLocation();

  const [user, setUser] = useState({
    Id: null,
    FirstName: null,
    LastName: null,
    Email: null,
    BillerCode: null,
    Role: null,
    Status: null,
  });

  const [isLoading, setIsLoading] = useState(true);

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    const token = localStorage.getItem('token');

    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/users/userdetails`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (data && typeof data.Id !== 'undefined') {
            setUser({
              Id: data.Id,
              FirstName: data.FirstName,
              LastName: data.LastName,
              Email: data.Email,
              BillerCode: data.BillerCode,
              Role: data.Role,
              Status: data.Status,
            });
          } else {
            setUser({
              Id: null,
              FirstName: null,
              LastName: null,
              Email: null,
              BillerCode: null,
              Role: null,
              Status: null,
            });
          }
          setIsLoading(false);
        });
    } else {
      setIsLoading(false);
    }
  }, []);

  if (isLoading) {
    return null;
  }

  const routes = [
    { path: '/dashboard', element: <DashboardContent /> },
    { path: '/billings', element: <BillingsContent /> },
    { path: '/payments', element: <PaymentsContent /> },
    { path: '/mainconso', element: <MainConso /> },  
    { path: '/clusterone', element: <ClusterOne /> },  
    { path: '/clustertwo', element: <ClusterTwo /> },
    { path: '/clusterthree', element: <ClusterThree /> },
    { path: '/users', element: <UsersContent /> },

  ];

  const isAuthenticated = user.Id !== null;
  const is404Page = routes.every((route) => route.path !== location.pathname);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <>
        {isAuthenticated && !is404Page ? <Sidebar /> : null}
        <Routes>
          <Route path="/" element={<Outlet />}>
            <Route index element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Registration />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<PageNotFound />} />
            {routes.map((route) => (
              <Route
                key={route.path}
                path={route.path}
                element={
                  <PrivateRoute
                    element={route.element}
                    authenticated={isAuthenticated}
                  />
                }
              />
            ))}
          </Route>
        </Routes>
      </>
    </UserProvider>
  );
}
